import os
import sys
from concurrent.futures import ThreadPoolExecutor

application_path = ""
if hasattr(sys, 'frozen'):
    application_path = os.path.dirname(sys.executable)
elif __file__:
    application_path = os.path.dirname(os.path.abspath(__file__))
sys.path.append(application_path)
print(f"当前项目路径为{application_path}")
# application_path = os.path.join(application_path, "SIFANG_PROJECT")
if not os.path.exists(application_path):
    os.mkdir(application_path)
if not os.path.exists(os.path.join(application_path, "Result")):
    os.mkdir(os.path.join(application_path, "Result"))
if not os.path.exists(os.path.join(application_path, "proxy")):
    os.mkdir(os.path.join(application_path, "proxy"))

if not os.path.exists(os.path.join(application_path, "logs")):
    os.mkdir(os.path.join(application_path, "logs"))


from Tuao.Start import Single
from UtilsApi.proxyFileUtils import ProxyFileClass
proxyObj = ProxyFileClass(application_path)

if __name__ == '__main__':
    try:
        with ThreadPoolExecutor(max_workers=1, thread_name_prefix="pro") as pool:
            pool.submit(lambda cxp: Single(*cxp), (application_path, proxyObj))

    except KeyboardInterrupt as e:
        print("手动终止了")

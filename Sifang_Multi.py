import sys
from concurrent.futures import ThreadPoolExecutor

from utils.Constant import ROOT_PATH
from utils.Constant import SIFANG_FILE_NAME
from utils.SingleTask import Single_SIFANG

sys.path.append(ROOT_PATH)
from model import getAllPhone
if __name__ == '__main__':
    listPhone = getAllPhone()
    with open(SIFANG_FILE_NAME,"r") as f:
        phoneList = f.read()

    phoneList = phoneList.split("\n")
    print(f"已成功运行的手机号有 {listPhone} , 本次不在运行")
    print(f"当前要运行的所有手机号有 { phoneList } ")
    phoneListFilter = [str(line).strip() for line in phoneList ]
    phoneListFilter = list(filter(lambda x:x not in listPhone,phoneListFilter))
    print(f"当前要运行的所有手机号有{phoneListFilter}")
    try:
        with ThreadPoolExecutor(max_workers=20, thread_name_prefix="pro") as pool:
            for line in phoneListFilter:
                if line in listPhone:
                    continue
                feature = pool.submit(Single_SIFANG, str(line))
    except KeyboardInterrupt as e:
        print("手动终止了")

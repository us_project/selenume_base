import os
import sys
from concurrent.futures import ThreadPoolExecutor

application_path = ""
if hasattr(sys, 'frozen'):
    application_path = os.path.dirname(sys.executable)
elif __file__:
    application_path = os.path.dirname(os.path.abspath(__file__))
sys.path.append(application_path)
print(f"当前项目路径为{application_path}")
# application_path = os.path.join(application_path, "SIFANG_PROJECT")
if not os.path.exists(application_path):
    os.mkdir(application_path)
if not os.path.exists(os.path.join(application_path, "Result")):
    os.mkdir(os.path.join(application_path, "Result"))
if not os.path.exists(os.path.join(application_path, "proxy")):
    os.mkdir(os.path.join(application_path, "proxy"))

if not os.path.exists(os.path.join(application_path, "logs")):
    os.mkdir(os.path.join(application_path, "logs"))





from SiFang.Start import Single_SIFANG
from UtilsApi.proxyFileUtils import ProxyFileClass


proxyObj = ProxyFileClass(application_path)

from modelNew import DbUtils

if __name__ == '__main__':
    listPhone = DbUtils(application_path).getAllPhone()

    with open(proxyObj.config["SIFANG_FILENAME"], "r") as f:
        phoneList = f.read()

    TempPhoneList = phoneList.split("\n")
    phoneList = [line.split(" ")[1] for line in TempPhoneList]

    print(f"已成功运行的手机号有{listPhone} ,本次不在运行")
    print(f"当前所有手机号有{phoneList}")
    phoneListFilter = [str(line).strip() for line in phoneList]
    phoneListFilter = list(filter(lambda x: x not in listPhone, phoneListFilter))
    print(f"当前要运行的所有手机号有{phoneListFilter}")

    try:
        with ThreadPoolExecutor(max_workers=proxyObj.config["MAX_WORK_COUNT"], thread_name_prefix="pro") as pool:
            for line in phoneListFilter:
                if line in listPhone:
                    continue
                pool.submit(lambda cxp: Single_SIFANG(*cxp), (application_path, proxyObj,str(line)))

    except KeyboardInterrupt as e:
        print("手动终止了")
# pyinstaller.exe -F .\SiFang.py -p D:\aaaa\NikeRegister
# pyinstaller.exe -F .\SiFang.py -p D:\aaaa\NikeRegister
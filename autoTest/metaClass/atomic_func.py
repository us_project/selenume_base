import copy
import os
import random
import threading
import time

import yaml
from selenium import webdriver
from selenium.common.exceptions import StaleElementReferenceException
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait

from utils.Constant import CHROME_OPTIONS
from utils.Constant import IP_Position
from utils.Constant import ISUPDATE_PASSWORD
from utils.Constant import LOG_DIR
from utils.Constant import PASSWORD
from utils.Constant import ROOT_PATH
from utils.Constant import URL
from utils.ProxyUtils import GetProxyFile
from utils.UA import getSingleUA
from utils.custumException import *
from utils.fileUtils import renderStr
from utils.birth import getRandomBirth
locak = threading.Lock()


class WebDriver:
    def __init__(self, phonenumber, log=None):
        self.imgCnt = 0
        self.register = "0"  # 是否已经注册
        self.name = phonenumber
        self.code = None  # 验证码
        self.seDriver = None
        # self.log = log

    def startChromDriver(self):
        chromeOptions = webdriver.ChromeOptions()
        # 使用代理登录
        proxyAddress, zipFile = GetProxyFile()
        assert os.path.exists(zipFile), "文件不存在"
        geo = {"latitude": IP_Position[proxyAddress]["lat"], "longitude": IP_Position[proxyAddress]["lon"],
               "accuracy": 100}
        tz = {"timezoneId": IP_Position[proxyAddress]["timezone"]}
        chromeOptions.add_argument(f"--window-size={random.randint(1500, 1920)},{random.randint(800, 1080)}")
        # chromeOptions.add_argument("--window-size=1920,1080")
        chromeOptions.add_argument("--ignore-certificate-errors")
        # chromeOptions.add_argument("--no-sandbox")
        chromeOptions.add_argument("--disable-gpu")
        chromeOptions.add_argument("--lang=zh-CN")
        UA = getSingleUA()
        chromeOptions.add_argument('user-agent=' + UA)
        # chromeOptions.add_argument('--incognito')  # 隐身模式（无痕模式）
        chromeOptions.add_argument('--disable-infobars')  # 禁用浏览器正在被自动化程序控制的提示
        chromeOptions.add_argument('--no-sandbox')

        chromeOptions.add_experimental_option('excludeSwitches', ['enable-automation'])  # 规避检测
        chromeOptions.add_argument('blink-settings=imagesEnabled=false')  # 不加载图片, 提升速度
        chromeOptions.add_extension(zipFile)
        # chromeOptions.add_argument('kiosk')
        # chromeOptions.add_argument("auto-open-devtools-for-tabs")
        # chromeOptions.add_experimental_option('useAutomationExtension', False)
        # chromeOptions.add_argument('lang=zh-CN,zh,zh-TW,en-US,en')

        # 是否需要弹出浏览器
        if not CHROME_OPTIONS:
            chromeOptions.add_argument("--headless")
        self.seDriver = webdriver.Chrome(options=chromeOptions)

        self.seDriver.execute_cdp_cmd("Page.addScriptToEvaluateOnNewDocument", {
            "source": """
            Object.defineProperty(navigator, 'webdriver', {
              get: () => false
            })
          """
        })
        # IP_Position[proxyAddress]
        # "lat": 39.0469,
        # "lon": -77.4903,
        # "timezone": "America/New_York",
        # "query": "185.230.248.190"

        self.seDriver.execute_cdp_cmd("Emulation.setGeolocationOverride", geo)
        self.seDriver.execute_cdp_cmd("Emulation.setTimezoneOverride", tz)
        self.seDriver.delete_all_cookies()
        print(f"当前使用代理 {proxyAddress=} 代理zip文 {zipFile=} \n{chromeOptions.arguments}")
        self.log.info(f'配置chrome浏览器，启动....')
        self.log.info(f" {proxyAddress=} 代理zip文 {zipFile=} {chromeOptions=}")

    def loadYmlDataAndRun(self, ymlFilePath, phone="", code="", password=PASSWORD):
        assert os.path.exists(ymlFilePath), f'{ymlFilePath}文件不存在'
        with open(ymlFilePath, 'r', encoding='utf-8') as f:
            fstepsRead = f.read()
        execSteps = yaml.load(renderStr(fstepsRead, phone=phone, code=code, password=self.password),
                              Loader=yaml.FullLoader)
        self.RunTest(execSteps)

    def StartRegister(self):
        if self.register == 1:
            yamlPath = "registerLogin.yml"
            # 已经注册过的
        elif self.register == 2:
            # 第一次注册
            yamlPath = "register.yml"
        else:
            self.screenShot()
            msg = "未知异常，有可能是Nike的网址xpath 发生了变化"
            self.log.info(msg)
            raise XpathError(msg)
        self.forgetPassword(self.code, yamlPath)
        self.checkIsSuccess()

    def checkProxyUrl(self, cnt=3):
        self.log.info(f"打开浏览器 {URL}")
        self.seDriver.get(URL)
        location = """(//button[contains(.,"登录")])[1]"""

        while cnt > 0:
            self.screenShot()
            self.seDriver.execute_script("return document.readyState")
            self.click(location, ignore=True)
            time.sleep(25)
            current_url = self.seDriver.current_url
            if "https://accounts.nike.com.cn/lookup" in current_url:
                self.screenShot()
                break
            else:
                self.seDriver.refresh()
                cnt -= 1
        self.screenShot()

        if cnt <= 0:
            raise ProxyChromeNetWorkError("有可能网络不通，退出，换下一个线程继续执行")
        self.log.info("当前点击登录进入输入手机号页面正常")

        cnt = 3
        while cnt > 0:
            username = "//input[@id='username']"
            _ele = self.seDriver.find_element(By.XPATH, username)
            _getValue = _ele.get_attribute('value')
            if "+86" in _getValue:

                break
            else:
                time.sleep(1)
                cnt -= 1
        self.screenShot()
        if cnt <= 0:
            raise ProxyChromeNetWorkError("没有成功获取到输入手机号的正确格式，有可能是网络问题")

    def quit(self):
        if self.seDriver:
            self.seDriver.quit()

    def checkIsSuccess(self):
        self.screenShot()
        self.log.info("等待页面加载完成...")
        cnt = 3
        while cnt > 0:
            if "complete" in str(self.seDriver.execute_script("return document.readyState")).lower():
                break
            else:
                time.sleep(2)
        self.log.info("页面加载完成...")
        time.sleep(4)
        errXpath = "(//div[contains(.,'验证码无效')])[last()]"

        if self.NodeExist(errXpath):
            self.screenShot()
            self.log.info("当前验证码无效")
            raise CODEInvalidException("验证码无效")

        self.screenShot()
        success = f"""//div[@role="alert" and contains(.,"新密码不可与先前使用过的密码相同")]|(// p[contains(., "Nike 会员起始时间")] | // h1[contains(text(), '你已成功登录。')]|//h1[contains(@aria-label,"是否要以 +86{self.name} 继续？")] |(//p[contains(.,"，您好") and contains(@aria-label,"您好！帐户和收藏")])[1])[last()] """
        if self.NodeExist(success):
            self.log.info(f"当前手机号注册成功! {self.name}")
            self.screenShot()
            return True

    def NodeExist(self,xpath):
        try:
            self.log.info("开始检查 xpath 是否存在 ")
            self.seDriver.find_element(By.XPATH, xpath)
        except Exception as e :
            return False
        else:
            return True


    def checkRegister(self):
        checkPath = os.path.join(ROOT_PATH, "config", "checkRegister.yml")
        self.loadYmlDataAndRun(checkPath, phone=self.name)
        location = "//h1[contains(text(),'你的密码是什么？')]|//h1[contains(text(),'验证你的电话号码并输入新密码。')]"
        result = self.sigleGetText(location)
        if result:  # 如果存在代表已经注册过
            if not ISUPDATE_PASSWORD:  # 如果
                #   desc: 如果已经注册过的,是否需要修改密码 true=> 修改密码  false => 不修改密码,直接退出
                self.log.info("当前模式您的配置文件选择了不修改密码,程序直接退出了")
                self.screenShot()
                raise RegisterError("当前模式您的配置文件选择了不修改密码,程序直接退出了")
            else:
                self.screenShot()
                self.click("//a[contains(text(),'忘记密码？')]", ignore=True)
                self.screenShot()
                self.register = 1
        else:
            self.register = 2
            self.screenShot()

    def forgetPassword(self, code, ymlPath):
        self.screenShot()
        checkPath = os.path.join(ROOT_PATH, "config", ymlPath)
        self.loadYmlDataAndRun(checkPath, phone=self.name, code=code, password=self.password)
        self.screenShot()

    def RunTest(self, listSteps):
        self.screenShot()
        for line in listSteps:
            if 'cmd' in line.keys():
                timeout = line['timeout'] if line.get('timeout') else 30
                cnt = line['cnt'] if line.get('cnt') else 5
                value = line['value'] if line.get('value') else ''
                location = line['location'] if line.get('location') else ''
                _type = line['_type'] if line.get('_type') else None
                ignore = line['ignore'] if line.get('ignore') else None
                if "getUrl" == line['cmd']:
                    self.seDriver.get(location)

                if "inputBirth" == line['cmd']:
                    self.inputBirth(location=location, value=value, timeout=timeout, delayTime=1, cnt=cnt)

                if "click" == line['cmd']:
                    self.click(location=location, timeout=timeout, delayTime=1, cnt=cnt, ignore=ignore)
                if "setValue" == line['cmd']:
                    self.setValue(location=location, value=value, timeout=timeout, cnt=cnt, _type=_type)
                if 'getText' == line['cmd']:
                    self.getText(location, timeout=timeout, cnt=5)
                if 'execApi' == line['cmd']:
                    funcName = line['location']
                    newDic = {k: v for k, v in line.items() if 'cmd' != k and 'location' != k and 'desc' != k}
                    self.call_children(funcName, newDic)
                self.screenShot()
            self.screenShot()
    def call_children(self, funcName, newDic):
        child_method = getattr(self, funcName)  # 获取子类的out()方法
        child_method(**newDic)  # 执行子类的out()方法

    def sigleGetText(self, location, timeout=30, cnt=5, ignore=None):
        try:
            self.ensureEleVisble(location, timeout)
            _ele = self.seDriver.find_element(By.XPATH, location)
            _text = _ele.text
        except Exception as e:
            self.log.info("没有获取到")
            return False
        else:
            self.log.debug(f"获得位置{location} 的值为：{_text}")
            return True

    def getText(self, location, timeout=30, cnt=5, ignore=None):
        self.log.info(f"正在尝试获取文字:{location}")
        count = copy.deepcopy(cnt)
        while cnt > 0:
            try:
                self.ensureEleVisble(location, timeout)
                _ele = self.seDriver.find_element(By.XPATH, location)
                _text = _ele.text
            except StaleElementReferenceException:
                self.log.error(f"第{count - cnt + 1}次尝试得到{location}的值失败")
                time.sleep(3)
                cnt -= 1
            except Exception as e:
                cnt -= 1
                if not ignore:
                    raise Exception(f"程序发生异常,请检查是否正确使用xpath,报错信息为{e}")
            else:
                self.log.debug(f"获得位置{location} 的值为：{_text}")
                return _text
        if not ignore:
            assert cnt > 0, "尝试获取文字失败"
        else:
            return True

    def ensureElePresent(self, location, timeout=30):
        assert location, '地址不能为空'
        _ele = WebDriverWait(self.seDriver, timeout).until(
            expected_conditions.presence_of_all_elements_located((By.XPATH, location)))
        return _ele

    def ensureEleVisble(self, location, timeout=30):
        assert location, '地址不能为空'
        _ele = WebDriverWait(self.seDriver, timeout).until(
            expected_conditions.visibility_of_element_located((By.XPATH, location)))
        return _ele

    def rightClick(self, location, timeout):
        """
            右键点击操作
        :return:
        """
        _ele = self.ensureElePresent(location=location, timeout=timeout)
        _action = ActionChains(self.seDriver)
        _action.context_click(_ele)
        _action.perform()

    def inputBirth(self, location, value, timeout=1, delayTime=1, cnt=3):
        self.log.info(f'现在开始在浏览器中输入值 value:{value} location: {location}')

        while cnt > 0:
            try:
                birthday = getRandomBirth()
                self.log.info("现在开始输入年月日")
                self.ensureElePresent(location, timeout)
                self.seDriver.find_element(By.XPATH, location).click()
                self.seDriver.find_element(By.XPATH, location).send_keys(birthday)
            except Exception as e:
                self.log.error(f"输入出生年月失败 ,value: {value} location: {location}， 错误消息：{str(e)}")
                time.sleep(3)
                cnt -= 1
            else:
                self.log.info(f"输入值{value}成功，location:{location}")
                break
        assert cnt > 0, f'多次尝试setValue 失败{location}'

    def setParam(self, location='', value='', timeout=30, cnt=0, _type=None):
        self.log.info(f'现在开始在浏览器中输入值 value:{value} location: {location}')
        while cnt > 0:
            try:
                self.ensureElePresent(location, timeout)
                _ele = self.seDriver.find_element(By.XPATH, location)
                _ele.send_keys(Keys.CONTROL, 'a')
                _ele.send_keys(Keys.DELETE)
                value = str(value)
                for line in value:
                    time.sleep(0.1)
                    _ele.send_keys(str(line))
                _getValue = _ele.get_attribute('value')
                assert value == _getValue, f"没有正确输入值{location}"
            except Exception as _:
                self.log.error(f"设置的值有问题 value: {value} location:{location}")
                time.sleep(3)
                cnt -= 1
            else:
                self.log.info(f"输入值{value}成功，location:{location}")
                break
            assert cnt > 0, f'多次尝试setValue 失败{location}'

    def setPhone(self, location='', value='', timeout=30, cnt=0, _type=None):
        self.log.info(f'现在开始在浏览器中输入值 手机号 value:{value} location: {location}')
        while cnt > 0:
            try:
                self.ensureElePresent(location, timeout)
                _ele = self.seDriver.find_element(By.XPATH, location)
                _ele.send_keys(Keys.CONTROL, 'a')
                _ele.send_keys(Keys.DELETE)
                value = str(value)

                for line in value:
                    time.sleep(0.1)
                    _ele.send_keys(str(line))

                _getValue = _ele.get_attribute('value')
                rightPhone = f"+86 {value[:3]} {value[3:7]} {value[7:12]}"
                assert rightPhone == _getValue, f"没有正确输入值{location}"

            except Exception as _:
                self.log.error(f"设置的值有问题 value: {value} location:{location}")
                time.sleep(3)
                cnt -= 1
            else:
                self.log.info(f"输入值{value}成功，location:{location}")
                break
            assert cnt > 0, f'多次尝试setValue 失败{location}'

    def setValue(self, location='', value='', timeout=30, cnt=0, _type=None):
        if _type == "phone":
            self.setPhone(location, value, timeout, cnt, _type)
        else:
            self.setParam(location, value, timeout, cnt, _type)

    def click(self, location, timeout=30, delayTime=1, cnt=5, ignore=None):
        """
            点击操作
        :param location:
        :param value:
        :param timeout:
        :param delayTime:
        :return:
        """
        if delayTime:
            time.sleep(delayTime)
        self.log.info(f"现在开始点击操作,location:{location}")
        while cnt > 0:
            try:
                clickable = self.seDriver.find_element(By.XPATH, location)
                clickable.click()
            except:
                self.log.error(f"点击操作失败...")
                time.sleep(delayTime)
                cnt -= 1
                if ignore:
                    self.log.info("没有获取到内容，这个是nike 的校验脚本， 可以忽略")
                    break
                    return False
            else:
                self.log.info(f"点击操作成功，location: {location}")
                break
                return True

        assert cnt > 0, f'尝试多次仍然没有找到这个位置,location:{location}'

    def executeJavaScript(self, script, *args):
        self.seDriver.execute_script(script, *args)

    def screenShot(self):
        if not self.seDriver:
            return
        self.imgCnt += 1
        filename = f"steps{self.imgCnt}.png"
        fileDir = os.path.join(LOG_DIR, self.name)
        os.makedirs(fileDir, exist_ok=True)
        filePath = os.path.join(fileDir, filename)
        self.seDriver.save_screenshot(filePath)

    def getUrl(self, url):
        self.seDriver.get(url)

    def closeUrl(self):
        self.seDriver.quit()


if __name__ == '__main__':
    driver = WebDriver("17316153750")
    driver.startChromDriver()

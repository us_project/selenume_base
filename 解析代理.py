import json
import os
import sys
import time
from concurrent.futures import ThreadPoolExecutor, as_completed
from random import choice

import requests

ROOT_PATH = ""
if hasattr(sys, 'frozen'):
    ROOT_PATH = os.path.dirname(sys.executable)
elif __file__:
    ROOT_PATH = os.path.dirname(os.path.abspath(__file__))
print(f"当前的工作地址是{ROOT_PATH}")


def getIPZone(ip, proxyList):
    print(f"开始解析IP {ip}")
    cnt = 5
    result = None
    while cnt > 0:
        try:
            ipProxy = choice(proxyList)
            proxyS = getRightProxy(ipProxy)
            a = requests.get(f"http://ipinfo.io/{ip}", proxies=proxyS)
            result = a.json()
        except Exception as e:
            print(f"正在重试:{ip}")
            cnt -= 1
        else:
            return {"ip": ip, "result": result}
    return False


def getRightProxy(temp_ip):
    temp_ip = temp_ip.split(":")
    return {"http": f"http://{temp_ip[2]}:{temp_ip[3]}@{temp_ip[0]}:{temp_ip[1]}"}


if __name__ == '__main__':

    proxy_path = os.path.join(ROOT_PATH, "proxy.txt")
    proxyLoc_Path = os.path.join(ROOT_PATH, "proxyLoc.json")
    proxy_readme = os.path.join(ROOT_PATH, "proxy_readme.txt")

    proxyLoc = {}
    if os.path.exists(proxyLoc_Path):
        with open(proxyLoc_Path, "r", encoding="utf-8") as f:
            proxyLoc = f.read()
            proxyLoc = json.loads(proxyLoc)

    with open(proxy_path, "r", encoding="utf-8") as f:
        proxyList = f.read().split("\n")
        proxyList = list(filter(None, proxyList))

        proxyData = list(set([line.split(":")[0] for line in proxyList]))
    start_time = time.time()
    with ThreadPoolExecutor(max_workers=50) as pool:
        results = [pool.submit(lambda cxp: getIPZone(*cxp), (line, proxyList)) for line in proxyData if
                   line not in proxyLoc]
        for result in as_completed(results):
            data = result.result()
            if data:
                proxyLoc[data["ip"]] = data["result"]

    with open(proxyLoc_Path, "w", encoding="utf-8") as f:
        f.write(json.dumps(proxyLoc, indent=4))
    end_time = time.time()
    with open(proxy_readme, "w", encoding="utf-8") as f:
        msg = f"代理IP 共有{len(proxyData)} 条, 解析成功{len(proxyLoc)},共耗时{end_time - start_time}秒"
        f.write(msg)

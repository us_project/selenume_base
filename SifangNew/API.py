import time

import requests

from UtilsApi.ExceptionUtils import *
from UtilsApi.LogUtils import GetLogger
from UtilsApi.WebUtils import WebDriver
from UtilsApi.proxyFileUtils import ProxyFileClass
from modelNew import DbUtils
from utils.ConstantEnum import CRUD_ENUM


class GetPhoneApi(WebDriver, ProxyFileClass, DbUtils):
    def __init__(self, phone=None, projectPath=None, obj=None, com_code=""):
        DbUtils.__init__(self, projectPath)
        self.com_code = "".join(list(filter(str.isdigit, com_code)))
        self.obj = obj if obj else ProxyFileClass(projectPath)
        self.projectPath = projectPath
        self.phone = phone
        self.log = None
        if phone:
            self.log = GetLogger(self.phone, self.projectPath).get_logger()
        if self.getExistPhone(self.phone):
            self.log.info(f"当前环境已经存在手机号{self.phone}")
            params={}
            params["phonenumber"] = self.phone
            params["comid"] = str(self.com_code)
            self.crudOperate(CRUD_ENUM.UPDATE.value, params)
            raise NotUseException("已经注册了")
        self.msgPre = ""
        self.baseAPI = self.obj.config["api"]

        self.project_id = None
        self.code = None
        self.seDriver = None

        self.password = self.obj.getPasswordRandom()
        self.sleepTime = 6


    def InsertData(self, params):
        assert self.phone, "当前没有获取到手机号码,程序退出"
        params["phonenumber"] = self.phone
        params["comid"] = self.com_code
        self.crudOperate(CRUD_ENUM.INSERT.value, params)  # 入库

    def updateStatus(self, params):
        assert self.phone, "当前没有获取到手机号码,程序退出"
        params["phonenumber"] = self.phone
        self.crudOperate(CRUD_ENUM.UPDATE.value, params)

    def release(self):
        pass

    def startChrom(self):
        # 调用谷歌浏览器
        WebDriver.__init__(self, self.phone, self.obj, self.log)
        self.startChromDriver()


class GetPhone(GetPhoneApi):
    def __init__(self, phone=None, projectPath=None, obj=None, com_code=None):
        GetPhoneApi.__init__(self, phone=phone, projectPath=projectPath, obj=obj, com_code=com_code)

    def getCode(self):
        self.log.info(f"{self.phone} 现在开始获取验证码 先等待10s ")
        time.sleep(10)
        api = self.baseAPI
        startTime = time.time()

        def isOverTime():
            if 180 < (time.time() - startTime):
                self.log.info("5分钟 验证码超时")
                raise getCodeTimeError("5分钟 验证码超时")

        while True:
            a = requests.get(api, proxies=self.obj.getRandomProxy())
            listPhone = a.json()
            result = list(
                filter(lambda x: str(x["com"]).strip() == self.com_code and "【NIKE中国】您的 NIKE 手机验证码是" in x["content"],
                       listPhone))
            if result:
                self.code = "".join(list(filter(str.isdigit, result[0]["content"])))
                break
            else:
                self.log.info(f"{self.phone} : 当前还没有获取到 手机验证码,等待5s 后重试")
            time.sleep(self.sleepTime)
            isOverTime()

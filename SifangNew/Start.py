from SifangNew.API import GetPhone
import traceback

from UtilsApi.ExceptionUtils import *
import time
def attempAlertChrome(instance):
    instance.quit()
    instance.startChrom()
    # 点击 登陆进入 输入手机号页面
    instance.checkProxyUrl()
    # 检查是否已经注册过,
    instance.checkRegister()

def Single(projectPath, proxyObj,phone_line):
    instance = None
    com_code , phone_number = phone_line.split(",")

    print(f""" Single  开始任务 {com_code}  {phone_number=} """)
    try:
        instance = GetPhone(projectPath=projectPath, obj=proxyObj, phone=phone_number,com_code=com_code)
        # 存手机号 入库 程序运行中 不释放
        instance.InsertData({"status": "5", "api": instance.obj.config["api"], "password": instance.password})

        cnt = 4
        while cnt >0:
            try:
                attempAlertChrome(instance)
            except ProxyException as e:
                time.sleep(2)
                cnt -= 1
            else:
                break

        # 等待验证码
        instance.getCode()
        instance.updateStatus({"code": str(instance.code)})
        # 浏览器开始注册
        instance.StartRegister()
        # 反馈值  1取码成功，2无验证码，3已注册 4其它
    except NotUseException as e:
        print("程序结束, 这个手机号已经注册过了用程序")

    except XpathError as e:
        instance.release()
        instance.updateStatus({"status": "3", "error": "Xpath 点击失败的" + str(e)})
        instance.quit()
    except RegisterError as e:  # # 已经注册过的,但是客户没有选择 忘记密码 而是直接退出的
        instance.release()

        instance.updateStatus({"status": "4", "error": "注册过,直接拉黑" + str(e)})
        instance.quit()
    except getCodeTimeError as e:
        instance.release()

        instance.updateStatus({"status": "3", "error": "验证吗超时" + str(e)})
        instance.quit()
    except Exception as e:
        traceback.print_exc()

        instance.release()
        instance.quit()
        if instance.phone:
            instance.updateStatus({"status": "3", "error": "页面代理问题" + str(e)})
        else:
            instance.updateStatus({"status": "3", "error": "API 获取不到手机号" + str(e)})
    else:
        # 正常执行 ,成功入库
        instance.updateStatus({"status": "1"})
        instance.quit()


import queue
import time
from concurrent.futures import ThreadPoolExecutor

workQueue = queue.Queue()


def pri(a):
    while True:
        a = workQueue.get()
        print(a)
        time.sleep(1)


def getStr(param, pool):
    if param == "5":
        pool.submit(pri, 5)
    print(param)


try:
    with ThreadPoolExecutor(max_workers=5) as pool:
        for line in range(10):
            feature = pool.submit(getStr, str(line), pool)

except KeyboardInterrupt as e:
    print("手动终止了，请稍后执行释放操作")

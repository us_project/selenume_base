import time

import requests
from bs4 import BeautifulSoup

from UtilsApi.ExceptionUtils import *
from UtilsApi.LogUtils import GetLogger
from UtilsApi.WebUtils import WebDriver
from UtilsApi.proxyFileUtils import ProxyFileClass
from modelNew import DbUtils
from utils.ConstantEnum import CRUD_ENUM


class GetPhoneApi(WebDriver, ProxyFileClass, DbUtils):
    def __init__(self, phone=None, projectPath=None, obj=None):
        DbUtils.__init__(self, projectPath)
        self.obj = obj if obj else ProxyFileClass(projectPath)
        self.projectPath = projectPath
        self.msgPre = ""
        self.baseAPI = self.obj.config["api"]
        # self.token = self.obj.config["token"]
        self.project_id = None
        self.phone = phone
        self.code = None
        self.seDriver = None
        self.log = None
        self.password = self.obj.getPasswordRandom()
        self.sleepTime = 6
        if phone:
            self.log = GetLogger(self.phone, self.projectPath).get_logger()

    def InsertData(self, params):
        assert self.phone, "当前没有获取到手机号码,程序退出"
        params["phonenumber"] = self.phone
        self.crudOperate(CRUD_ENUM.INSERT.value, params)  # 入库

    def updateStatus(self, params):
        assert self.phone, "当前没有获取到手机号码,程序退出"
        params["phonenumber"] = self.phone
        self.crudOperate(CRUD_ENUM.UPDATE.value, params)

    # def getUser(self):
    #     api = f"""{self.baseAPI}/sms/api/userinfo?token={self.token}"""
    #     response = requests.get(api).json()
    #     if response["code"] == 0:
    #         print(f"当前User 信息{response}")

    def getCode(self):
        self.log.info("现在开始获取验证码 先等待10s ")
        time.sleep(10)
        api = f"""{self.baseAPI}"""
        startTime = time.time()

        def isOverTime():
            if 180 < (time.time() - startTime):
                self.log.info("5分钟 验证码超时")
                raise getCodeTimeError("5分钟 验证码超时")

        while True:
            response = requests.get(api, proxies=self.obj.getRandomProxy()).json()
            phoneCode = f"{self.phone[0:2]}****{self.phone[7:]}"
            data = list(filter(lambda x: "nike" in str(x).lower() and phoneCode in x["simnum"], response))
            if data:
                self.code = "".join(list(filter(str.isdigit, data[0]["content"])))
                return
            time.sleep(self.sleepTime)
            isOverTime()

    def release(self):
        pass

    def startChrom(self):
        # 调用谷歌浏览器
        WebDriver.__init__(self, self.phone, self.obj, self.log)
        self.startChromDriver()


class GetPhoneSifangApiChinese(GetPhoneApi):
    def __init__(self, phone=None, projectPath=None, obj=None):
        GetPhoneApi.__init__(self, phone=phone, projectPath=projectPath, obj=obj)

    def getCode(self):
        self.log.info("现在开始获取验证码 先等待10s ")
        time.sleep(10)
        api = f"""{self.baseAPI}"""
        startTime = time.time()

        def isOverTime():
            if 180 < (time.time() - startTime):
                self.log.info("5分钟 验证码超时")
                raise getCodeTimeError("5分钟 验证码超时")

        while True:
            response = requests.get(api, proxies=self.obj.getRandomProxy())
            if response.status_code == 200:
                soup = BeautifulSoup(response.text, 'html.parser')
                for link in soup.find_all('tr'):
                    if "来自" not in link.text:
                        tdList = link.find_all('td')
                        if self.phone[:3] + "****" + self.phone[7:] == tdList[1].text and "【NIKE中国】" in tdList[2].text:
                            self.code = "".join(list(filter(str.isdigit, tdList[2].text)))
                            self.log.info(f"手机号验证码获取成功 {self.phone} : {self.code}")
                            return
            time.sleep(self.sleepTime)
            isOverTime()

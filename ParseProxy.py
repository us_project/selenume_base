import concurrent
import json
import os
import sys
import time
from concurrent.futures import ThreadPoolExecutor

import requests

ROOT_PATH = ""
if hasattr(sys, 'frozen'):
    ROOT_PATH = os.path.dirname(sys.executable)
elif __file__:
    ROOT_PATH = os.path.dirname(os.path.abspath(__file__))
print(f"当前的工作地址是{ROOT_PATH}")
proxyTxt = os.path.join(ROOT_PATH, "proxy" ,"proxy.txt")
RightProxyJSON_PATH = os.path.join(ROOT_PATH, "proxy", "Proxy_Right.txt")
RightProxy_PATH = os.path.join(ROOT_PATH, "proxy", "Proxy_Used.txt")
ErrProxy_PATH = os.path.join(ROOT_PATH, "proxy", "Proxy_Error.txt")


def getIPaddr(proxylist):
    proxy = proxylist.split(":")
    ip = proxy[0]
    cnt = 15
    print(f"开始解析IP地址{proxylist}")
    while cnt > 0:
        url = f"http://ip-api.com/json/{ip}"
        try:
            data = requests.get(url, timeout=10).json()
        except Exception as e:
            cnt -= 1
            time.sleep(2)
        else:
            if data["status"] == "success":
                data = {"lat": data["lat"], "lon": data["lon"], "timezone": data["timezone"], "query": data["query"]}
                print(f"解析完成, URL {proxylist}可用")
                return proxylist, data
            else:
                return None, proxylist
    if cnt <= 0:
        return None, proxylist


if __name__ == '__main__':
    right = list()
    with open(proxyTxt, "r",) as f:
        HTTP_PROXY_POOL_TXT = f.read()
    HTTP_PROXY_POOL = list(filter(None, HTTP_PROXY_POOL_TXT.split("\n")))
    with ThreadPoolExecutor(max_workers=20) as pool:
        futures = {}
        # with 中可以使用 变量htmls
        for url in HTTP_PROXY_POOL:
            feture = pool.submit(getIPaddr, url)
            futures[feture] = url
        # as_completed 哪个任务先完成就先返回哪个, 顺序是不固定的
        for feture in concurrent.futures.as_completed(futures):
            url = futures[feture]
            right.append(feture.result())

    data = list(filter(lambda x: x[0], right))
    err = list(filter(lambda x: not x[0], right))
    err = [line[1] for line in err]

    rightIp = {line[0]: line[1] for line in data}
    rightProxy = [line[1] for line in data]
    with open(RightProxyJSON_PATH, "w") as f:
        f.write(json.dumps(rightIp, ensure_ascii=True, indent=4))
    with open(RightProxy_PATH, "w") as f:
        f.write("\n".join(rightIp))
    with open(ErrProxy_PATH, "w") as f:
        f.write("\n".join(err))

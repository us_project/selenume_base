import sys
from concurrent.futures import ThreadPoolExecutor

from utils.Constant import MAX_WORK
from utils.Constant import ROOT_PATH
from utils.Constant import RUNNING_OPTIONS
from utils.LogConfig import log, getLogName
from utils.SingleTask import StartGetCode

sys.path.append(ROOT_PATH)
log = log(getLogName())

log.info("现在开始多线程运行了")

try:
    with ThreadPoolExecutor(max_workers=MAX_WORK) as pool:
        for line in range(RUNNING_OPTIONS):
            feature = pool.submit(StartGetCode, str(line))

except KeyboardInterrupt as e:
    print("手动终止了，请稍后执行释放操作")

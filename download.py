import os

import xlwt

from model import Phone
from model import Session
from model import model_to_dict
from utils.Constant import ROOT_PATH

session = Session()
mark = session.query(Phone).all()

resultPath = os.path.join(ROOT_PATH, "result.xls")
data = model_to_dict(mark)
# 1. 第一次注册成功 2. 改密码成功注册 3.失败 4.由于参数控制,手机不改密直接退出的 5. 程序正在运行中的

dic = {"1": "注册成功", "2": "改密成功", "3": "失败", "4": "不改密直接退出", "5": "注册过程中"}
release = {"0": "没有释放", "1": "已释放", "2": "接口不需要释放"}


def generate_excel():
    work_book = xlwt.Workbook(encoding='utf-8')
    data.sort(key=lambda x: (x['status']))  # 按照科目和成绩进行双排序
    sheet = work_book.add_sheet("success")
    sheet.write(0, 0, '手机号')
    sheet.write(0, 1, '密码')
    sheet.write(0, 2, '类型')
    sheet.write(0, 3, '是否释放')
    sheet.write(0, 4, '错误原因')
    sheet.write(0, 5, 'API')
    cnt = 0

    for k, v in enumerate(data):
        cnt += 1
        sheet.write(cnt, 0, v["phonenumber"])
        sheet.write(cnt, 1, v["password"])
        sheet.write(cnt, 2, dic[v["status"]])

        releaseStr = "无需释放" if v["status"] == "1" else release[v["release"]]
        releaseStr = "已上报" if v["api"] else releaseStr

        sheet.write(cnt, 3, releaseStr)
        sheet.write(cnt, 4, v["error"])
        sheet.write(cnt, 5, v["api"])

    work_book.save(f'{resultPath}')

if __name__ == '__main__':

    generate_excel()

import copy
import os
import random
import time

import yaml
from selenium import webdriver
from selenium.common.exceptions import StaleElementReferenceException
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait

from UtilsApi.ExceptionUtils import *


class WebDriver:
    def __init__(self, phonenumber, obj, log):
        self.imgCnt = 0
        self.register = "0"  # 是否已经注册
        self.name = phonenumber
        self.code = None  # 验证码
        self.seDriver = None
        self.ProxyClassObj = obj
        self.log = log

        self.register_flag =False
    def startChromDriver(self, proxyList):
        # '119.96.192.169:23033:3t638u:q8cguo'
        chromeOptions = webdriver.ChromeOptions()
        # 使用代理登录
        proxyAddress, zipFile = self.ProxyClassObj.GetProxyAddAndFile(proxyList)
        assert os.path.exists(zipFile), "文件不存在"

        ip = proxyAddress.split(":")[0]
        geo = {"latitude": float(proxyList[ip]["loc"].split(",")[0]),
               "longitude": float(proxyList[ip]["loc"].split(",")[1]),
               "accuracy": 100}
        tz = {"timezoneId": proxyList[ip]["timezone"]}
        chromeOptions.add_argument(f"--window-size={random.randint(1500, 1920)},{random.randint(800, 1080)}")
        # chromeOptions.add_argument("--window-size=1920,1080")
        chromeOptions.add_argument("--ignore-certificate-errors")
        # chromeOptions.add_argument("--no-sandbox")
        chromeOptions.add_argument("--disable-gpu")
        chromeOptions.add_argument("--lang=zh-CN")
        UA = self.ProxyClassObj.getRandomUA()
        chromeOptions.add_argument('user-agent=' + UA)
        # chromeOptions.add_argument('--incognito')  # 隐身模式（无痕模式）
        chromeOptions.add_argument('--disable-infobars')  # 禁用浏览器正在被自动化程序控制的提示
        chromeOptions.add_argument('--no-sandbox')

        chromeOptions.add_experimental_option('excludeSwitches', ['enable-automation'])  # 规避检测
        chromeOptions.add_argument('blink-settings=imagesEnabled=false')  # 不加载图片, 提升速度
        chromeOptions.add_extension(zipFile)
        # chromeOptions.add_argument('kiosk')
        # chromeOptions.add_argument("auto-open-devtools-for-tabs")
        # chromeOptions.add_experimental_option('useAutomationExtension', False)
        # chromeOptions.add_argument('lang=zh-CN,zh,zh-TW,en-US,en')

        # 是否需要弹出浏览器
        # if not self.ProxyClassObj.config.get("CHROME_OPTIONS"):
        # chromeOptions.add_argument("--headless")
        self.seDriver = webdriver.Chrome(options=chromeOptions)

        self.seDriver.execute_cdp_cmd("Page.addScriptToEvaluateOnNewDocument", {
            "source": """
            Object.defineProperty(navigator, 'webdriver', {
              get: () => false
            })
          """
        })
        self.seDriver.execute_cdp_cmd("Emulation.setGeolocationOverride", geo)
        self.seDriver.execute_cdp_cmd("Emulation.setTimezoneOverride", tz)
        self.seDriver.delete_all_cookies()
        self.log.info(f"当前使用代理 {proxyAddress=} 代理zip文 {zipFile=} \n{chromeOptions.arguments}")
        self.log.info(f'配置chrome浏览器，启动....')
        self.log.info(f" {proxyAddress=} 代理zip文 {zipFile=} {chromeOptions=}")

    def loadYmlDataAndRun(self, ymlFilePath, phone="", code="", password=None):
        if not password:
            password = self.ProxyClassObj.getPasswordRandom()

        assert os.path.exists(ymlFilePath), f'{ymlFilePath}文件不存在'
        with open(ymlFilePath, 'r', encoding='utf-8') as f:
            fstepsRead = f.read()
        execSteps = yaml.load(self.ProxyClassObj.renderStr(fstepsRead, phone=phone, code=code, password=password),
                              Loader=yaml.FullLoader)
        self.RunTest(execSteps)

    def StartRegister(self):
        yamlPath = "reRegister.yml" if self.register_flag else "register.yml"
        self.forgetPassword(self.code, yamlPath)
        self.checkIsSuccess()

    def waitComplet(self):
        while True:
            complete = self.seDriver.execute_script("return document.readyState") == "complete"
            if complete:
                break
            else:
                time.sleep(3)
        time.sleep(2)

    def checkProxyUrl(self, cnt=3):
        URL = "https://www.nike.com.cn/help"
        self.log.info(f"打开浏览器 {URL}")
        self.seDriver.get(URL)
        time.sleep(3)
        location = """(//button[contains(.,"登录")])[1]"""
        # while cnt > 0:
        self.screenShot()
        self.waitComplet()
        time.sleep(5)
        current_url = self.seDriver.current_url
        self.waitComplet()

        while "https://accounts.nike.com.cn/lookup" not in current_url:
            current_url = self.seDriver.current_url
            if "https://accounts.nike.com.cn/lookup" not in current_url:
                self.click(location)
                self.waitComplet()
                time.sleep(3)
                current_url = self.seDriver.current_url
            current_url = self.seDriver.current_url
            self.screenShot()
        # if "https://accounts.nike.com.cn/lookup" in current_url:
        #     self.log.info("正确跳转到登陆页面")
        # else:
        #     self.click(location)

        self.screenShot()
        cnt = 3
        while cnt > 0:
            username = "//input[@id='username']"
            _ele = self.seDriver.find_element(By.XPATH, username)
            _getValue = _ele.get_attribute('value')
            if "+86" in _getValue:
                break
            else:
                time.sleep(1)
                cnt -= 1
        self.screenShot()
        if cnt <= 0:
            raise ProxyChromeNetWorkError("没有成功获取到输入手机号的正确格式，有可能是网络问题")

    def quit(self):
        if self.seDriver:
            self.seDriver.quit()

    def checkIsSuccess(self):
        self.screenShot()
        cnt = 3
        while cnt > 0:
            if "complete" in str(self.seDriver.execute_script("return document.readyState")).lower():
                break
            else:
                time.sleep(1)

        success = f"""(// p[contains(., "Nike 会员起始时间")] | // h1[contains(text(), '你已成功登录。')]|//h1[contains(@aria-label,"是否要以 +86{self.name} 继续？")] |(//p[contains(.,"，您好") and contains(@aria-label,"您好！帐户和收藏")])[1])[last()] """
        result = self.getText(success, timeout=5, cnt=1, ignore=True)
        if result == True:
            self.seDriver.get("https://www.nike.com.cn/member/profile/")
            result = self.getText(success, timeout=5, cnt=1, ignore=True)
            if result:
                self.screenShot()

                return True
            if not self.seDriver.find_element(By.XPATH, "//input[@id='privacyTerms']").is_selected():
                self.click("//input[@id='privacyTerms']")

            self.click("""//button[contains(.,"下一步")]|//button[contains(.,"继续")]""")
            self.getText("""//p[contains(.,"Nike 会员起始时间")]""")
            self.screenShot()

        else:
            self.screenShot()

            return True

    def checkRegister(self):
        checkPath = os.path.join(self.ProxyClassObj.filePath, "config", "checkRegister.yml")
        self.loadYmlDataAndRun(checkPath, phone=self.name)
        # location = "//h1[contains(text(),'你的密码是什么？')]|//h1[contains(text(),'验证你的电话号码并输入新密码。')]"
        if "生日时获得 Nike 会员奖励。" in self.seDriver.page_source:
            self.register = 2
            self.screenShot()
            return
        if "解析服务器响应时发生错误" in self.seDriver.page_source:
            raise ProxyException("代理IP 有问题,解析服务器响应时发生错误")
        # "生日时获得 Nike 会员奖励。" in self.seDriver.page_source or self.sigleGetText(location)
        location = "//h1[contains(text(),'你的密码是什么？')]|//h1[contains(text(),'验证你的电话号码并输入新密码。')]"

        if "你的密码是什么？" in self.seDriver.page_source or "验证你的电话号码并输入新密码。" in self.seDriver.page_source or self.sigleGetText(
                location):
            self.log.info(f"当前号码{self.name}已经注册过了,直接拉黑")
            self.register_flag = True
            self.click("//a[contains(text(),'忘记密码？')]", ignore=True)

    # raise RegisterError(f"当前号码{self.name}已经注册过了,直接拉黑")
        else:
            self.screenShot()

    def forgetPassword(self, code, ymlPath):
        self.screenShot()
        checkPath = os.path.join(self.ProxyClassObj.filePath, "config", ymlPath)
        self.loadYmlDataAndRun(checkPath, phone=self.name, code=code, password=self.password)
        self.screenShot()

    def RunTest(self, listSteps):
        self.screenShot()
        for line in listSteps:
            if 'cmd' in line.keys():
                timeout = line['timeout'] if line.get('timeout') else 30
                cnt = line['cnt'] if line.get('cnt') else 5
                value = line['value'] if line.get('value') else ''
                location = line['location'] if line.get('location') else ''
                _type = line['_type'] if line.get('_type') else None
                ignore = line['ignore'] if line.get('ignore') else None
                if "getUrl" == line['cmd']:
                    self.seDriver.get(location)

                if "inputBirth" == line['cmd']:
                    self.inputBirth(location=location, value=value, timeout=timeout, delayTime=1, cnt=cnt)

                if "click" == line['cmd']:
                    self.click(location=location, timeout=timeout, delayTime=1, cnt=cnt, ignore=ignore)
                if "setValue" == line['cmd']:
                    self.setValue(location=location, value=value, timeout=timeout, cnt=cnt, _type=_type)
                if 'getText' == line['cmd']:
                    self.getText(location, timeout=timeout, cnt=5)
                if 'execApi' == line['cmd']:
                    funcName = line['location']
                    newDic = {k: v for k, v in line.items() if 'cmd' != k and 'location' != k and 'desc' != k}
                    self.call_children(funcName, newDic)
                self.screenShot()
                self.waitComplet()

    def call_children(self, funcName, newDic):
        child_method = getattr(self, funcName)  # 获取子类的out()方法
        child_method(**newDic)  # 执行子类的out()方法

    def sigleGetText(self, location, timeout=30, cnt=5, ignore=None):
        try:
            self.ensureEleVisble(location, timeout)
            _ele = self.seDriver.find_element(By.XPATH, location)
            _text = _ele.text
        except Exception as e:
            self.log.info("没有获取到")
            return False
        else:
            self.log.debug(f"获得位置{location} 的值为：{_text}")
            return True

    def getText(self, location, timeout=30, cnt=5, ignore=None):
        self.log.info(f"正在尝试获取文字:{location}")
        count = copy.deepcopy(cnt)
        while cnt > 0:
            try:
                self.ensureEleVisble(location, timeout)
                _ele = self.seDriver.find_element(By.XPATH, location)
                _text = _ele.text
            except StaleElementReferenceException:
                self.log.error(f"第{count - cnt + 1}次尝试得到{location}的值失败")
                time.sleep(3)
                cnt -= 1
            except Exception as e:
                cnt -= 1
                if not ignore:
                    raise Exception(f"程序发生异常,请检查是否正确使用xpath,报错信息为{e}")
            else:
                self.log.debug(f"获得位置{location} 的值为：{_text}")
                return _text
        if not ignore:
            assert cnt > 0, "尝试获取文字失败"
        else:
            return True

    def ensureElePresent(self, location, timeout=30):
        assert location, '地址不能为空'
        _ele = WebDriverWait(self.seDriver, timeout).until(
            expected_conditions.presence_of_all_elements_located((By.XPATH, location)))
        return _ele

    def ensureEleVisble(self, location, timeout=30):
        assert location, '地址不能为空'
        _ele = WebDriverWait(self.seDriver, timeout).until(
            expected_conditions.visibility_of_element_located((By.XPATH, location)))
        return _ele

    def rightClick(self, location, timeout):
        """
            右键点击操作
        :return:
        """
        _ele = self.ensureElePresent(location=location, timeout=timeout)
        _action = ActionChains(self.seDriver)
        _action.context_click(_ele)
        _action.perform()

    def inputBirth(self, location, value, timeout=1, delayTime=1, cnt=3):
        self.log.info(f'现在开始在浏览器中输入值 value:{value} location: {location}')

        while cnt > 0:
            try:
                birthday = self.ProxyClassObj.getRandomBirth()
                self.log.info("现在开始输入年月日")
                self.ensureElePresent(location, timeout)
                self.seDriver.find_element(By.XPATH, location).click()
                self.seDriver.find_element(By.XPATH, location).send_keys(birthday)
            except Exception as e:
                self.log.error(f"输入出生年月失败 ,value: {value} location: {location}， 错误消息：{str(e)}")
                time.sleep(3)
                cnt -= 1
            else:
                self.log.info(f"输入值{value}成功，location:{location}")
                break
        assert cnt > 0, f'多次尝试setValue 失败{location}'

    def setParam(self, location='', value='', timeout=30, cnt=0, _type=None):
        self.log.info(f'现在开始在浏览器中输入值 value:{value} location: {location}')
        while cnt > 0:
            try:
                self.ensureElePresent(location, timeout)
                _ele = self.seDriver.find_element(By.XPATH, location)
                _ele.send_keys(Keys.CONTROL, 'a')
                _ele.send_keys(Keys.DELETE)
                value = str(value)
                for line in value:
                    time.sleep(0.1)
                    _ele.send_keys(str(line))
                _getValue = _ele.get_attribute('value')
                assert value == _getValue, f"没有正确输入值{location}"
            except Exception as _:
                self.log.error(f"设置的值有问题 value: {value} location:{location}")
                time.sleep(3)
                cnt -= 1
            else:
                self.log.info(f"输入值{value}成功，location:{location}")
                break
            assert cnt > 0, f'多次尝试setValue 失败{location}'

    def setPhone(self, location='', value='', timeout=30, cnt=0, _type=None):
        self.log.info(f'现在开始在浏览器中输入值 手机号 value:{value} location: {location}')
        while cnt > 0:
            try:
                self.ensureElePresent(location, timeout)
                _ele = self.seDriver.find_element(By.XPATH, location)
                _ele.send_keys(Keys.CONTROL, 'a')
                _ele.send_keys(Keys.DELETE)
                value = str(value)

                for line in value:
                    time.sleep(0.1)
                    _ele.send_keys(str(line))

                _getValue = _ele.get_attribute('value')
                rightPhone = f"+86 {value[:3]} {value[3:7]} {value[7:12]}"
                assert rightPhone == _getValue, f"没有正确输入值{location}"

            except Exception as _:
                self.log.error(f"设置的值有问题 value: {value} location:{location}")
                time.sleep(3)
                cnt -= 1
            else:
                self.log.info(f"输入值{value}成功，location:{location}")
                break
            assert cnt > 0, f'多次尝试setValue 失败{location}'

    def setValue(self, location='', value='', timeout=30, cnt=0, _type=None):
        if _type == "phone":
            self.setPhone(location, value, timeout, cnt, _type)
        else:
            self.setParam(location, value, timeout, cnt, _type)

    def click(self, location, timeout=30, delayTime=1, cnt=5, ignore=None):
        """
            点击操作
        :param location:
        :param value:
        :param timeout:
        :param delayTime:
        :return:
        """
        if delayTime:
            time.sleep(delayTime)
        self.log.info(f"现在开始点击操作,location:{location}")
        while cnt > 0:
            try:
                clickable = self.seDriver.find_element(By.XPATH, location)
                clickable.click()
            except:
                self.log.error(f"点击操作失败...")
                time.sleep(delayTime)
                cnt -= 1
                if ignore:
                    self.log.info("没有获取到内容，这个是nike 的校验脚本， 可以忽略")
                    break
                    return False
            else:
                self.log.info(f"点击操作成功，location: {location}")
                break
                return True

        assert cnt > 0, f'尝试多次仍然没有找到这个位置,location:{location}'

    def executeJavaScript(self, script, *args):
        self.seDriver.execute_script(script, *args)

    def screenShot(self):
        if not self.seDriver:
            return

        self.imgCnt += 1
        filename = f"steps{self.imgCnt}.png"
        fileDir = os.path.join(os.path.join(self.ProxyClassObj.filePath, "logs"), self.name)
        os.makedirs(fileDir, exist_ok=True)
        filePath = os.path.join(fileDir, filename)
        self.seDriver.save_screenshot(filePath)

    def getUrl(self, url):
        self.seDriver.get(url)

    def closeUrl(self):
        self.seDriver.quit()


if __name__ == '__main__':
    driver = WebDriver("17316153750", None, None)
    driver.startChromDriver()

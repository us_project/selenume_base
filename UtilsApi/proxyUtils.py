
import random
class GetProxyPhone():
    def __init__(self):
        self.proxyIP = getProxy(HTTP_PROXY_POOL)
        print(f"您当前获取到的代理IP 地址是 {self.proxyIP}")
        self.proxies = None
        self.phone = None

    def parseProxy(self, ip):
        # 185.230.249.218:63938:leaf:vMocmJoc
        proxyList = ip["http"].split(":")
        self.proxies = {"http": f"http://{proxyList[2]}:{proxyList[3]}@{proxyList[0]}:{proxyList[1]}"}
        print(f"使用代理{self.proxies}")
        return self.proxies

    def getRandomProxy(self):
        ip = random.choice(self.proxyIP)
        return self.parseProxy(ip)

    def InsertData(self, params):
        assert self.phone, "当前没有获取到手机号码,程序退出"
        params["phonenumber"] = self.phone
        crudOperate(CRUD_ENUM.INSERT.value, params)  # 入库

    def updateStatus(self, params):
        assert self.phone, "当前没有获取到手机号码,程序退出"
        params["phonenumber"] = self.phone
        crudOperate(CRUD_ENUM.UPDATE.value, params)

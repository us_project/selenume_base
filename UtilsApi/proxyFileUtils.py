import os
import random
import secrets
import string
import zipfile
from random import choice

import yaml
from jinja2 import Template

from utils.fileUtils import render


class ProxyFileClass(object):
    def __init__(self, path, configFileName=None):
        self.filePath = path
        self.configFileName = configFileName

        self.proxyPath = os.path.join(path, "proxy")
        self.proxyFile = os.path.join(self.proxyPath, "proxy.txt")
        # self.Proxy_RightPath = os.path.join(self.proxyPath, 'Proxy_Right.txt')
        # self.Proxy_USED_PATH = os.path.join(self.proxyPath, 'Proxy_Used.txt')
        self.proxyExtensionsPath = os.path.join(path, "proxy", 'chrome-proxy-extensions')

        # self.IP_Position = self.getIpPosition()
        self.proxyList = self.getAllProxy()

        self.UA_LIST = self.getUAList()
        self.CreateProxyFile()
        # self.getIpPosition()
        self.config = self.getConfig()
        print(f"当前的代理池有{self.proxyList}")

    def getRandomBirth(self):
        return f"19" + f"{random.randint(80, 99)}-0{random.randint(1, 9)}-{random.randint(11, 28)}"

    def getConfig(self):
        configname = "config.yml" if not self.configFileName else self.configFileName
        yaml_file = open(os.path.join(self.proxyPath, configname), 'r', encoding="utf-8")
        data = yaml.load(yaml_file, Loader=yaml.FullLoader)
        dic = dict()
        for line in data:
            dic[line["key"]] = line["value"]
        return dic

    def getPasswordRandom(self):
        def getRamdomStr():
            return secrets.choice(string.ascii_letters)

        pre = [str(getRamdomStr()).upper() for line in range(random.randint(3, 6))]
        middle = [str(getRamdomStr()).lower() for line in range(random.randint(4, 6))]
        number = [str(random.randint(1, 9)) for line in range(random.randint(3, 6))]
        pre.extend(number)
        pre.extend(middle)
        random.shuffle(pre)
        return "".join(pre)

    def getRandomProxy(self):
        """
        随机取一条代理IP
        """
        temp_ip = choice(self.proxyList)
        temp_ip = temp_ip.split(":")
        return {"http": f"http://{temp_ip[2]}:{temp_ip[3]}@{temp_ip[0]}:{temp_ip[1]}"}


    def GetProxyAddAndFile(self, proxyList):
        proxys = list(filter(lambda x: x.split(":")[0] in proxyList, self.proxyList))
        proxy = choice(proxys)

        proxylist = proxy.split(":")
        proxyType = "http"
        file = "_".join(proxylist).replace(".", "_").replace(":", "_")
        extension_file_path = os.path.join(self.proxyExtensionsPath, f'{proxyType}_{file}.zip')
        if os.path.exists(extension_file_path):
            self.get_chrome_proxy_extension(proxy)
        return proxy, extension_file_path

    def getAllProxy(self):
        with open(self.proxyFile, "r", encoding='UTF-8') as f:
            proxyList = list(filter(None, f.read().split("\n")))
            return [str(line).strip() for line in proxyList]

    def CreateProxyFile(self):
        if self.proxyList:
            for line in self.proxyList:
                self.get_chrome_proxy_extension(line)

    def getUAList(self):
        UA = """
        Chrome 9	Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36
        Chrome	Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36
        Microsoft Edge	Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36 Edge/17.17134
        Chrome 8	Mozilla/5.0 (Windows NT 10.0; ) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36
        Chrome 8	Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36
        Chrome 9	Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.61 Safari/537.36
        Chrome 8	Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36
        Chrome 9	Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36
        Chrome	Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36
        Firefox 7	Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0"""
        ua = list(filter(None, UA.split("\n")))
        ua = [str(line).strip() for line in ua if line]
        return ua

    def getRandomUA(self):
        # 随机取一个UA
        return choice(self.UA_LIST)

    def render(self, filepath, **kwargs):
        assert kwargs, "参数传递错误"
        with open(filepath, "r", encoding="UTF-8") as f:
            tempStr = f.read()
            return Template(tempStr).render(kwargs)

    def renderStr(self, value, **kwargs):
        assert kwargs, "参数传递错误"

        return Template(value).render(kwargs)

    def get_chrome_proxy_extension(self, proxy):
        """
        获取一个Chrome代理扩展,里面配置有指定的代理(带用户名密码认证)
        proxy - 指定的代理,格式: username:password@ip:port
        """
        # "42.101.9.225:4848:cgtod1666711398:apu0g7"
        proxylist = proxy.split(":")
        ip = proxylist[0]
        port = proxylist[1]
        username = proxylist[2]
        password = proxylist[3]
        proxyType = "http"
        if proxy:
            # 提取代理的各项参数
            # 创建一个定制Chrome代理扩展(zip文件)
            if not os.path.exists(self.proxyExtensionsPath):
                os.mkdir(self.proxyExtensionsPath)

            file = "_".join(proxylist).replace(".", "_").replace(":", "_")
            extension_file_path = os.path.join(self.proxyExtensionsPath, f'{proxyType}_{file}.zip')
            # extension_file_path = f"{proxyType}_{file}.zip"
            if not os.path.exists(extension_file_path):
                # 扩展文件不存在，创建
                zf = zipfile.ZipFile(extension_file_path, mode='w')

                with open(os.path.join(self.proxyPath, 'manifest.json'), "r", encoding="UTF-8") as f:
                    manifest = f.read()

                zf.writestr('manifest.json', manifest)
                backgroundFile = os.path.join(self.proxyPath, "background.js.template")
                backgroundFileValue = render(backgroundFile, proxyType=proxyType, username=username, password=password,
                                             ip=ip, port=port)

                zf.writestr("background.js", backgroundFileValue)
                zf.close()
            assert os.path.exists(extension_file_path), "代理文件不存在"
            return extension_file_path
        else:
            raise Exception('Invalid proxy format. Should be username:password@ip:port')

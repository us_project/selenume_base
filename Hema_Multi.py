import sys
from concurrent.futures import ThreadPoolExecutor

from utils.Constant import MAX_WORK_SECOND
from utils.Constant import ROOT_PATH
from utils.Constant import RUNNING_OPTIONS_SECOND
from utils.SingleTask import SecondTask

sys.path.append(ROOT_PATH)


print("现在开始多线程运行了")
if __name__ == '__main__':
    try:
        with ThreadPoolExecutor(max_workers=MAX_WORK_SECOND) as pool:
            for line in range(RUNNING_OPTIONS_SECOND):
                feature = pool.submit(SecondTask, str(line))

    except KeyboardInterrupt as e:
        print("手动终止了，请稍后执行释放操作")

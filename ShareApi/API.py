import time

import requests

from UtilsApi.ExceptionUtils import *
from UtilsApi.WebUtils import WebDriver
from UtilsApi.dataUtils import getNumber
from UtilsApi.proxyFileUtils import ProxyFileClass
from modelNew import DbUtils
from utils.ConstantEnum import CRUD_ENUM
from UtilsApi.LogUtils import GetLogger

class GetPhoneApi(WebDriver, ProxyFileClass,DbUtils):
    def __init__(self, phone=None,projectPath=None, obj=None):
        DbUtils.__init__(self,projectPath)
        self.obj = obj if obj else ProxyFileClass(projectPath)
        self.projectPath = projectPath
        self.msgPre = ""
        self.baseAPI = self.obj.config["api"]
        self.token = self.obj.config["token"]
        self.project_id = self.obj.config["project_id"]
        self.phone = phone

        self.code = None
        self.seDriver = None
        self.log = None
        self.password = self.obj.getPasswordRandom()
        self.sleepTime = 6

    def InsertData(self, params):
        assert self.phone, "当前没有获取到手机号码,程序退出"
        params["phonenumber"] = self.phone
        self.crudOperate(CRUD_ENUM.INSERT.value, params)  # 入库

    def updateStatus(self, params):
        assert self.phone, "当前没有获取到手机号码,程序退出"
        params["phonenumber"] = self.phone
        self.crudOperate(CRUD_ENUM.UPDATE.value, params)

    def getUser(self):
        api = f"""{self.baseAPI}/sms/api/userinfo?token={self.token}"""
        response = requests.get(api).json()
        if response["code"] == 0:
            print(f"当前User 信息{response}")

    def getPhone(self):
        if self.phone:
            self.log = GetLogger(self.phone, self.projectPath).get_logger()
            return
        print("现在开始获取手机号")
        api = f"""{self.baseAPI}/sms/api/getPhone?token={self.token}&sid={self.project_id}&numsize=1"""
        while True:
            print("现在开始获取手机号")
            response = requests.get(api).json()

            if response["code"] == 0:
                self.phone = response["phone"]
                self.log = GetLogger(self.phone, self.projectPath).get_logger()
                self.log.info(f"获取手机号 {self.phone} 成功 API: {api}")
                return
            elif response["code"] == -4 or response["code"] ==-5 :
                # {"msg":"余额不足请充值","code":-4}
                print(f"您的余额已不足 程序退出..{api=} {response=}")
                raise MonnyException("您的余额已不足")
            else:
                print(f"not get Phone , later...{api=} {response=}")

            print(f"没有获取到手机号,休眠{self.sleepTime}s 后重试")
            time.sleep(self.sleepTime)

    def getCode(self):
        self.log.info("现在开始获取验证码 先等待10s ")
        time.sleep(10)
        api = f"""{self.baseAPI}/sms/api/getMessage?token={self.token}&sid={self.project_id}&phone={self.phone}"""
        startTime = time.time()

        def isOverTime():
            if 180 < (time.time() - startTime):
                self.log.info("5分钟 验证码超时")
                raise getCodeTimeError("5分钟 验证码超时")

        while True:
            response = requests.get(api).json()
            # {"msg": "success", "code": 0, "sms": "您的短信为10083"}
            if response["code"] == 0:
                self.code = getNumber(response["sms"])
                self.log.info(f"获取手机号验证码 成功: {self.phone}  :{self.code}")
                return
            else:
                self.log.info(f"获取手机号验证码 error ,5s 后重试 {api=} {response=}")
                time.sleep(self.sleepTime)
            isOverTime()

    def release(self):
        #     http://api.lxy8.net/sms/api/addBlacklist?token=登陆返回的令牌&sid=项目ID&phone=手机号码
        self.log.info(f"现在开始加黑手机{self.phone}")
        if self.phone:
            self.screenShot()
            self.log.info(f"现在开始加黑手机{self.phone}")
            api = f"""{self.baseAPI}/sms/api/addBlacklist?token={self.token}&sid={self.project_id}&phone={self.phone}"""
            response = requests.get(api).json()
            if response["code"] == 0:
                self.log.info(f"加黑手机号成功 {self.phone}")
            else:
                self.log.info(f"加黑手机号失败 {self.phone}")

    def startChrom(self):
        # 调用谷歌浏览器
        WebDriver.__init__(self, self.phone, self.obj,self.log)
        self.startChromDriver()

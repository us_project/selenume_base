import xlwt
import os
import sys
from modelNew import DbUtils ,Phone
application_path = ""
if hasattr(sys, 'frozen'):
    application_path = os.path.dirname(sys.executable)
elif __file__:
    application_path = os.path.dirname(os.path.abspath(__file__))
sys.path.append(application_path)
print(f"当前项目路径为{application_path}")
# application_path = os.path.join(application_path, "SIFANG_PROJECT")
if not os.path.exists(application_path):
    os.mkdir(application_path)
if not os.path.exists(os.path.join(application_path, "Result")):
    os.mkdir(os.path.join(application_path, "Result"))
if not os.path.exists(os.path.join(application_path, "proxy")):
    os.mkdir(os.path.join(application_path, "proxy"))

if not os.path.exists(os.path.join(application_path, "logs")):
    os.mkdir(os.path.join(application_path, "logs"))

obj  = DbUtils(application_path)
# COMNone,COMNone,


mark = obj.Session().query(Phone).all()
import datetime
a= datetime.datetime.now()
filename = str(a).replace(" ","").replace(":","_").replace(".","_").replace("-","")
resultPath = os.path.join(application_path, f"result_{filename}.xls")
data = obj.model_to_dict(mark)

dic = {"1": "注册成功", "2": "改密成功", "3": "失败", "4": "不改密直接退出", "5": "注册过程中"}
release = {"0": "没有释放", "1": "已释放", "2": "接口不需要释放"}


def generate_excel():
    work_book = xlwt.Workbook(encoding='utf-8')
    data.sort(key=lambda x: (x['status']))  # 按照科目和成绩进行双排序
    sheet = work_book.add_sheet("success")
    sheet.write(0, 0, '手机号')
    sheet.write(0, 1, '密码')
    sheet.write(0, 2, '类型')
    sheet.write(0, 3, '是否释放')
    sheet.write(0, 4, '错误原因')
    sheet.write(0, 5, 'API')
    sheet.write(0, 6, '创建时间')
    cnt = 0

    for k, v in enumerate(data):
        cnt += 1
        sheet.write(cnt, 0,f'COM{v["comid"]},{ v["phonenumber"]}')
        sheet.write(cnt, 1, v["password"])
        sheet.write(cnt, 2, dic[v["status"]])
        releaseStr = "无需释放" if v["status"] == "1" else release[v["release"]]
        releaseStr = "已上报" if v["api"] else releaseStr
        sheet.write(cnt, 3, releaseStr)
        sheet.write(cnt, 4, v["error"])
        sheet.write(cnt, 5, v["api"])
        sheet.write(cnt, 6, v["beginTime"])

    work_book.save(f'{resultPath}')
    print(f"导出的文件地址是{resultPath}")
if __name__ == '__main__':

    generate_excel()
# pyinstaller.exe -F .\GetResult.py -p C:\Users\npb\Pictures\Project\NikeRegister
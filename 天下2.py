import os
import sys
from concurrent.futures import ThreadPoolExecutor

application_path = ""
if hasattr(sys, 'frozen'):
    application_path = os.path.dirname(sys.executable)
elif __file__:
    application_path = os.path.dirname(os.path.abspath(__file__))
sys.path.append(application_path)
print(f"当前项目路径为{application_path}")
# application_path = os.path.join(application_path, "SIFANG_PROJECT")
if not os.path.exists(application_path):
    os.mkdir(application_path)
if not os.path.exists(os.path.join(application_path, "Result")):
    os.mkdir(os.path.join(application_path, "Result"))
if not os.path.exists(os.path.join(application_path, "proxy")):
    os.mkdir(os.path.join(application_path, "proxy"))

if not os.path.exists(os.path.join(application_path, "logs")):
    os.mkdir(os.path.join(application_path, "logs"))

from Tianxia2.Start import Single
from UtilsApi.proxyFileUtils import ProxyFileClass
import requests

proxyObj = ProxyFileClass(application_path)


# def getToken():
#     api = f"{proxyObj.config['api']}/user/doLogin?username={proxyObj.config['USERNAME']}&password={proxyObj.config['PASSWORD']}"
#     res = requests.get(api, proxies=proxyObj.getRandomProxy())
#     if res.status_code == 200:
#         data = res.json()
#         if data["success"]:
#             token = data["data"]["result"]["token"]
#             return token



if __name__ == '__main__':
    # token = getToken()
    # print(f"登陆成功, token={token}")
    # assert token, "登陆失败程序退出"

    try:
        with ThreadPoolExecutor(max_workers=proxyObj.config["MAX_WORK_COUNT"], thread_name_prefix="pro") as pool:
            for line in range(proxyObj.config["RUNNING_OPTIONS"]):
                pool.submit(lambda cxp: Single(*cxp), (application_path, proxyObj, line))

    except KeyboardInterrupt as e:
        print("手动终止了")
# pyinstaller.exe -F .\SiFang.py -p D:\aaaa\NikeRegister
# pyinstaller.exe -F .\天下3.py -p C:\Users\npb\Pictures\Project\NikeRegister

import sys
from concurrent.futures import ThreadPoolExecutor

from utils.Constant import MAX_WORK_YN
from utils.Constant import RUNNING_OPTIONS_YN
from utils.Constant import ROOT_PATH
from utils.SingleTask import Single_YN

sys.path.append(ROOT_PATH)

if __name__ == '__main__':
    print("现在开始多线程运行了")
    try:
        with ThreadPoolExecutor(max_workers=MAX_WORK_YN, thread_name_prefix="pro") as pool:
            for line in range(RUNNING_OPTIONS_YN):
                feature = pool.submit(Single_YN, str(line))
    except KeyboardInterrupt as e:
        print("手动终止了")

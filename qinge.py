import json
import os
import sys
from concurrent.futures import ThreadPoolExecutor

application_path = ""
if hasattr(sys, 'frozen'):
    application_path = os.path.dirname(sys.executable)
elif __file__:
    application_path = os.path.dirname(os.path.abspath(__file__))
sys.path.append(application_path)
print(f"当前项目路径为{application_path}")
# application_path = os.path.join(application_path, "SIFANG_PROJECT")
if not os.path.exists(application_path):
    os.mkdir(application_path)
if not os.path.exists(os.path.join(application_path, "Result")):
    os.mkdir(os.path.join(application_path, "Result"))
if not os.path.exists(os.path.join(application_path, "proxy")):
    os.mkdir(os.path.join(application_path, "proxy"))

if not os.path.exists(os.path.join(application_path, "logs")):
    os.mkdir(os.path.join(application_path, "logs"))

from Tianxia.Start import Single_TIANXIA
from UtilsApi.proxyFileUtils import ProxyFileClass

with  open(os.path.join(application_path, "proxy", "proxyLoc_copy.json"), "r", encoding="utf-8") as f:
    proxy_str = f.read()
    proxyList = json.loads(proxy_str)
configFileName = "config_qinge.yml"

proxyObj = ProxyFileClass(application_path, configFileName=configFileName)
if __name__ == '__main__':

    try:
        with ThreadPoolExecutor(max_workers=proxyObj.config["MAX_WORK_COUNT"], thread_name_prefix="pro") as pool:
            for line in range(proxyObj.config["RUNNING_OPTIONS"]):
                pool.submit(lambda cxp: Single_TIANXIA(*cxp),
                            (application_path, proxyObj, line, configFileName, proxyList))
    except KeyboardInterrupt as e:
        print("手动终止了")

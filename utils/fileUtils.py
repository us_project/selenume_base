from jinja2 import Template


def render(filepath, **kwargs):
    assert kwargs, "参数传递错误"
    with open(filepath, "r") as f:
        tempStr = f.read()
        return Template(tempStr).render(kwargs)

def renderStr(value, **kwargs):
    assert kwargs, "参数传递错误"

    return Template(value).render(kwargs)


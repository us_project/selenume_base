import sys

import requests

from utils.Constant import API_PROXY_QICAI
from utils.Constant import API_PROXY_FENG_YUN
from utils.Constant import ROOT_PATH
from utils.Constant import FENGYUN_TOKEN
from utils.UserApiFdc import GetPhoneCode
from utils.UserApiFdc import GetPhoneCode2
from utils.UserApiFdc import Get_2023
from utils.UserApiFdc import GetPhoneCodeQicai
from utils.UserApiFdc import GetPhoneCode_Fengyun
from utils.UserApiFdc import GetSIFANG
from utils.UserApiFdc import GetYN
from utils.custumException import *
from utils.getPassword import getPassword

sys.path.append(ROOT_PATH)


def StartGetCode(param):
    print(f"""开始任务{param}""")
    instance = None

    try:
        instance = GetPhoneCode()
        instance.login()  # 登陆 api 获取 token
        # instance.loadUserinfo()  # 登陆 api 获取 token
        # instance.loadUserWallet()  # 登陆 api 获取 token
        instance.getPhone()  # 登陆 api 获取 手机号
        instance.InsertData({"status": "5"})
        # 存手机号 入库 程序运行中 不释放
        # 浏览器打开
        instance.startChrom()
        # 点击 登陆进入 输入手机号页面
        instance.checkProxyUrl()
        # 检查是否已经注册过,
        instance.checkRegister()
        # 等待验证码
        instance.getCode()
        instance.updateStatus({"code": str(instance.code)})
        # 浏览器开始注册
        instance.StartRegister()

    except XpathError as e:
        instance.releasePhone()
        instance.updateStatus({"status": "3", "error": "Xpath 点击失败的"})
        instance.quit()
    except RegisterError as e:  # # 已经注册过的,但是客户没有选择 忘记密码 而是直接退出的
        instance.releasePhone()

        instance.updateStatus({"status": "4", "error": "由于参数控制,手机不改密直接退出的"})
        instance.quit()

    except requests.exceptions.RequestException as e:
        instance.releasePhone()
        print(f"代理不通{str(e)}")
        instance.updateStatus({"status": "3", "error": f"代理不通{str(e)}"})

        instance.quit()
    except ProxyError as e:
        instance.releasePhone()
        instance.updateStatus({"status": "3", "error": f"代理不通{str(e)}"})
        print(f"代理池访问不通 {str(e)}")
        instance.quit()
    except ProxyChromeNetWorkError as e:
        print(str(e))
        instance.releasePhone()
        instance.updateStatus({"status": "3", "error": str("浏览器加载页面不正常,手机已释放") + str(e)})
        instance.quit()
    except Exception as e:
        instance.releasePhone()
        instance.updateStatus({"status": "3", "error": "页面代理问题"})
        instance.quit()
    else:
        # 正常执行 ,成功入库
        instance.updateStatus({"status": "1"})
        instance.quit()


def SecondTask(param):
    instance = None
    print(f""" 第二种API 开始任务{param}""")

    try:
        instance = GetPhoneCode2()
        instance.password = getPassword()
        instance.login()  # 登陆 api 获取 token
        instance.getPhone()  # 登陆 api 获取 手机号
        # 存手机号 入库 程序运行中 不释放
        instance.InsertData({"status": "5", "api": "http://35.78.66.8:8000", "password": instance.password})
        # 浏览器打开
        instance.startChrom()
        # 点击 登陆进入 输入手机号页面
        instance.checkProxyUrl()
        # 检查是否已经注册过,
        instance.checkRegister()
        # 等待验证码
        instance.getCode()
        instance.updateStatus({"code": str(instance.code)})
        # 浏览器开始注册
        instance.StartRegister()

    except XpathError as e:
        instance.updateStatus({"status": "3", "error": "Xpath 点击失败的" + str(e)})
        instance.quit()
        instance.reportResult(4)
    except RegisterError as e:  # # 已经注册过的,但是客户没有选择 忘记密码 而是直接退出的
        instance.reportResult(3)
        instance.updateStatus({"status": "4", "error": "由于参数控制,手机不改密直接退出的" + str(e)})
        instance.quit()
    except getCodeTimeError as e:
        instance.updateStatus({"status": "3", "error": "验证吗超时" + str(e)})
        instance.reportResult(2)
        instance.quit()
    except Exception as e:
        instance.updateStatus({"status": "3", "error": "页面代理问题" + str(e)})
        instance.reportResult(4)
        instance.quit()
    else:
        # 正常执行 ,成功入库
        instance.updateStatus({"status": "1"})
        instance.reportResult(1)
        instance.quit()


def Single_Qicai(param):
    instance = None
    print(f""" 七彩API 开始任务{param}""")
    try:
        instance = GetPhoneCodeQicai()
        instance.password = getPassword()
        instance.getMoney()  # 登陆 api 获取 手机号
        instance.getPhone()  # 登陆 api 获取 手机号
        # 存手机号 入库 程序运行中 不释放
        instance.InsertData({"status": "5", "api": f"{API_PROXY_QICAI}", "password": instance.password})
        # 浏览器打开
        instance.startChrom()
        # 点击 登陆进入 输入手机号页面
        instance.checkProxyUrl()
        # 检查是否已经注册过,
        instance.checkRegister()
        # 等待验证码
        instance.getCode()
        instance.updateStatus({"code": str(instance.code)})
        # 浏览器开始注册
        instance.StartRegister()
        # 反馈值  1取码成功，2无验证码，3已注册 4其它

    except XpathError as e:
        instance.updateStatus({"status": "3", "error": "Xpath 点击失败的" + str(e)})
        instance.quit()
        instance.reportResult(4)
    except RegisterError as e:  # # 已经注册过的,但是客户没有选择 忘记密码 而是直接退出的
        instance.reportResult(3)
        instance.updateStatus({"status": "4", "error": "由于参数控制,手机不改密直接退出的" + str(e)})
        instance.quit()
    except getCodeTimeError as e:
        instance.updateStatus({"status": "3", "error": "验证吗超时" + str(e)})
        instance.reportResult(2)
        instance.quit()
    except Exception as e:
        instance.updateStatus({"status": "3", "error": "页面代理问题" + str(e)})
        instance.reportResult(4)
        instance.quit()
    else:
        # 正常执行 ,成功入库
        instance.updateStatus({"status": "1"})
        instance.reportResult(1)
        instance.quit()

def Single_2023(param):
    instance = None
    print(f""" 七彩API 开始任务{param}""")
    try:
        instance = Get_2023()
        instance.password = getPassword()
        instance.getPhone()  # 登陆 api 获取 手机号
        # 存手机号 入库 程序运行中 不释放
        instance.InsertData({"status": "5", "api": f"{API_PROXY_QICAI}", "password": instance.password})
        # 浏览器打开
        instance.startChrom()
        # 点击 登陆进入 输入手机号页面
        instance.checkProxyUrl()
        # 检查是否已经注册过,
        instance.checkRegister()
        # 等待验证码
        instance.getCode()
        instance.updateStatus({"code": str(instance.code)})
        # 浏览器开始注册
        instance.StartRegister()
        # 反馈值  1取码成功，2无验证码，3已注册 4其它

    except XpathError as e:
        instance.updateStatus({"status": "3", "error": "Xpath 点击失败的" + str(e)})
        instance.quit()
    except RegisterError as e:  # # 已经注册过的,但是客户没有选择 忘记密码 而是直接退出的
        instance.updateStatus({"status": "4", "error": "由于参数控制,手机不改密直接退出的" + str(e)})
        instance.quit()
    except getCodeTimeError as e:
        instance.updateStatus({"status": "3", "error": "验证吗超时" + str(e)})
        instance.quit()
    except Exception as e:
        instance.updateStatus({"status": "3", "error": "页面代理问题" + str(e)})
        instance.quit()
    else:
        # 正常执行 ,成功入库
        instance.updateStatus({"status": "1"})
        instance.quit()


def Single_FENGYUN(param):
    instance = None
    print(f""" 风云API  开始任务{param}""")
    try:
        instance = GetPhoneCode_Fengyun(token=FENGYUN_TOKEN)
        instance.password = getPassword()
        instance.getPhone()  # 登陆 api 获取 手机号

        if str(instance.phone).startswith("192"):
            raise PhoneInvalidException("手机号无效")

        # 存手机号 入库 程序运行中 不释放
        instance.InsertData({"status": "5", "api": f"{API_PROXY_FENG_YUN}", "password": instance.password})
        # 浏览器打开
        instance.startChrom()
        # 点击 登陆进入 输入手机号页面
        instance.checkProxyUrl()
        # 检查是否已经注册过,
        instance.checkRegister()
        # 等待验证码
        instance.getCode()
        instance.updateStatus({"code": str(instance.code)})
        # 浏览器开始注册
        instance.StartRegister()
        # 反馈值  1取码成功，2无验证码，3已注册 4其它
        instance.screenShot()
    except XpathError as e:
        instance.updateStatus({"status": "3", "error": "Xpath 点击失败的" + str(e)})
        instance.quit()
        instance.screenShot()
    except RegisterError as e:  # # 已经注册过的,但是客户没有选择 忘记密码 而是直接退出的
        instance.reportResult(4)
        instance.updateStatus({"status": "4", "error": "由于参数控制,手机不改密直接退出的" + str(e)})
        instance.quit()
        instance.screenShot()
    except getCodeTimeError as e:
        instance.updateStatus({"status": "3", "error": "验证吗超时" + str(e)})
        instance.quit()
        instance.screenShot()
    except CODEInvalidException as e:
        instance.screenShot()
        instance.updateStatus({"status": "3", "error": str(e)})
        instance.reportResult(3)
        instance.quit()
    except PhoneInvalidException as e:
        instance.reportResult(5)
    except Exception as e:
        instance.quit()
        if instance.phone:
            instance.updateStatus({"status": "3", "error": "页面代理问题" + str(e)})
        else:
            instance.updateStatus({"status": "3", "error": "API 获取不到手机号" + str(e)})
        instance.screenShot()
    else:
        # 正常执行 ,成功入库
        instance.updateStatus({"status": "1"})
        instance.reportResult(6)
        instance.quit()
        instance.screenShot()
    finally:
        instance.screenShot()



def Single_YN(param):
    instance = None
    print(f""" 风云API  开始任务{param}""")
    try:
        instance = GetYN()
        instance.password = getPassword()
        instance.getPhone()  # 登陆 api 获取 手机号
        # 存手机号 入库 程序运行中 不释放
        instance.InsertData({"status": "5", "api": f"{API_PROXY_FENG_YUN}", "password": instance.password})
        # 浏览器打开
        instance.startChrom()
        # 点击 登陆进入 输入手机号页面
        instance.checkProxyUrl()
        # 检查是否已经注册过,
        instance.checkRegister()
        # 等待验证码
        instance.getCode()
        instance.updateStatus({"code": str(instance.code)})
        # 浏览器开始注册
        instance.StartRegister()
        # 反馈值  1取码成功，2无验证码，3已注册 4其它

    except XpathError as e:
        instance.updateStatus({"status": "3", "error": "Xpath 点击失败的" + str(e)})
        instance.quit()
        instance.release()
    except RegisterError as e:  # # 已经注册过的,但是客户没有选择 忘记密码 而是直接退出的
        instance.release()
        instance.updateStatus({"status": "4", "error": "由于参数控制,手机不改密直接退出的" + str(e)})
        instance.quit()
    except getCodeTimeError as e:
        instance.updateStatus({"status": "3", "error": "验证吗超时" + str(e)})
        instance.release()
        instance.quit()
    except Exception as e:
        instance.quit()
        if instance.phone:
            instance.updateStatus({"status": "3", "error": "页面代理问题" + str(e)})
        else:
            instance.updateStatus({"status": "3", "error": "API 获取不到手机号" + str(e)})
        instance.release()
    else:
        # 正常执行 ,成功入库
        instance.updateStatus({"status": "1"})
        instance.quit()


def Single_SIFANG(param):
    instance = None
    print(f""" SIFANG_API  开始任务{param}""")
    try:
        instance = GetSIFANG(param)
        instance.password = getPassword()
        # 存手机号 入库 程序运行中 不释放
        instance.InsertData({"status": "5", "api": f"SIFANG", "password": instance.password})
        # 浏览器打开
        instance.startChrom()
        # 点击 登陆进入 输入手机号页面
        instance.checkProxyUrl()
        # 检查是否已经注册过,
        instance.checkRegister()
        # 等待验证码
        instance.getCode()
        instance.updateStatus({"code": str(instance.code)})
        # 浏览器开始注册
        instance.StartRegister()
        # 反馈值  1取码成功，2无验证码，3已注册 4其它

    except XpathError as e:
        instance.updateStatus({"status": "3", "error": "Xpath 点击失败的" + str(e)})
        instance.quit()
    except RegisterError as e:  # # 已经注册过的,但是客户没有选择 忘记密码 而是直接退出的
        instance.updateStatus({"status": "4", "error": "由于参数控制,手机不改密直接退出的" + str(e)})
        instance.quit()
    except getCodeTimeError as e:
        instance.updateStatus({"status": "3", "error": "验证吗超时" + str(e)})
        instance.quit()
    except PhoneExistException as e:
        print(e)
        instance.quit()
    except API_NOT_RESPONSE as e:
        instance.deleteData({})
    except Exception as e:
        instance.quit()
        if instance.phone:
            instance.updateStatus({"status": "3", "error": "页面代理问题" + str(e)})
        else:
            instance.updateStatus({"status": "3", "error": "API 获取不到手机号" + str(e)})
    else:
        # 正常执行 ,成功入库
        instance.updateStatus({"status": "1"})
        instance.quit()




if __name__ == '__main__':
    Single_FENGYUN("123")

import os
import json
from random import shuffle

import yaml
ymlpath = "C:\\Nike\\config.yml"
def getConfigYml(ymlpath):
    with open(ymlpath, 'r', encoding="utf-8") as f:
        fstepsReadcont = f.read()
    configData = yaml.load(fstepsReadcont, Loader=yaml.FullLoader)
    return configData


def getAllKeys(data):
    tmpDic = dict()
    for line in data:
        tmpDic[line["key"]] = line["value"]
    return tmpDic


def getAttrValue(keys, data):
    tmpDic = dict()
    for line in data:
        tmpDic[line["key"]] = line["value"]
    return tmpDic




CONFIGDATA = getConfigYml(ymlpath)

ALL_KEYS_DICT = getAllKeys(CONFIGDATA)

ITEMID = ALL_KEYS_DICT["ITEMID"]  # getAttrValue("ITEMID", CONFIGDATA)
username = ALL_KEYS_DICT["username"]  # getAttrValue("username", CONFIGDATA)
password = ALL_KEYS_DICT["password"]  # getAttrValue("password", CONFIGDATA)
username_type2 = ALL_KEYS_DICT["username_type2"]  # getAttrValue("username_type2", CONFIGDATA)
password_type2 = ALL_KEYS_DICT["password_type2"]  # getAttrValue("password_type2", CONFIGDATA)

MAX_WORK = ALL_KEYS_DICT["MAX_WORK"]  # getAttrValue("MAX_WORK", CONFIGDATA)
MAX_WORK_SECOND = ALL_KEYS_DICT["MAX_WORK_SECOND"]  # getAttrValue("MAX_WORK_SECOND", CONFIGDATA)
RUNNING_OPTIONS_SECOND = ALL_KEYS_DICT["RUNNING_OPTIONS_SECOND"]  # getAttrValue("RUNNING_OPTIONS_SECOND", CONFIGDATA)
TIMEOUT = ALL_KEYS_DICT["TIMEOUT"]  # getAttrValue("TIMEOUT", CONFIGDATA)
PASSWORD = ALL_KEYS_DICT["PASSWORD"]  # getAttrValue("PASSWORD", CONFIGDATA)
API_PROXY_TYPE1 = ALL_KEYS_DICT["API_PROXY_TYPE1"]  # getAttrValue("API_PROXY_TYPE1", CONFIGDATA)
API_PROXY_TYPE2 = ALL_KEYS_DICT["API_PROXY_TYPE2"]  # getAttrValue("API_PROXY_TYPE2", CONFIGDATA)
ITEMID_type2 = ALL_KEYS_DICT["ITEMID_type2"]  # getAttrValue("ITEMID_type2", CONFIGDATA)

URL = ALL_KEYS_DICT["URL"]  # getAttrValue("URL", CONFIGDATA)
ROOT_PATH = ALL_KEYS_DICT["ROOT_PATH"]  # getAttrValue("ROOT_PATH", CONFIGDATA)
PHONETYPE = ALL_KEYS_DICT["PHONETYPE"]  # getAttrValue("PHONETYPE", CONFIGDATA)
CHROME_OPTIONS = ALL_KEYS_DICT["CHROME_OPTIONS"]  # getAttrValue("CHROME_OPTIONS", CONFIGDATA)
RUNNING_OPTIONS = ALL_KEYS_DICT["RUNNING_OPTIONS"]  # getAttrValue("RUNNING_OPTIONS", CONFIGDATA)
ISUPDATE_PASSWORD = ALL_KEYS_DICT["ISUPDATE_PASSWORD"]  # getAttrValue("ISUPDATE_PASSWORD", CONFIGDATA)
ITEMID_QICAI = ALL_KEYS_DICT["ITEMID_QICAI"]  # getAttrValue("ITEMID_QICAI", CONFIGDATA)
username_QICAI = ALL_KEYS_DICT["username_QICAI"]  # getAttrValue("username_QICAI", CONFIGDATA)
password_QICAI = ALL_KEYS_DICT["password_QICAI"]  # getAttrValue("password_QICAI", CONFIGDATA)
API_PROXY_QICAI = ALL_KEYS_DICT["API_PROXY_QICAI"]  # getAttrValue("API_PROXY_QICAI", CONFIGDATA)
MAX_WORK_QICAI = ALL_KEYS_DICT["MAX_WORK_QICAI"]  # getAttrValue("MAX_WORK_QICAI", CONFIGDATA)
RUNNING_OPTIONS_QICAI = ALL_KEYS_DICT["RUNNING_OPTIONS_QICAI"]

username_FENG_YUN = ALL_KEYS_DICT["username_FENG_YUN"]
password_FENG_YUN = ALL_KEYS_DICT["password_FENG_YUN"]
API_PROXY_FENG_YUN = ALL_KEYS_DICT["API_PROXY_FENG_YUN"]
MAX_WORK_FENG_YUN = ALL_KEYS_DICT["MAX_WORK_FENG_YUN"]
RUNNING_OPTIONS_FENG_YUN = ALL_KEYS_DICT["RUNNING_OPTIONS_FENG_YUN"]
FENGYUN_TOKEN = ALL_KEYS_DICT["FENGYUN_TOKEN"]


API_PROXY_YN = ALL_KEYS_DICT["API_PROXY_YN"]
MAX_WORK_YN = ALL_KEYS_DICT["MAX_WORK_YN"]
RUNNING_OPTIONS_YN = ALL_KEYS_DICT["RUNNING_OPTIONS_YN"]
YN_TOKEN = ALL_KEYS_DICT["YN_TOKEN"]
YN_PROJECTID = ALL_KEYS_DICT["YN_PROJECTID"]
SIFANG_FILE_NAME = ALL_KEYS_DICT["SIFANG_FILE_NAME"]


# - key: API_PROXY_YN
#   value: "http://8.218.175.112:9898"
#   desc: YN 获取手机号的方式
#
#
# - key: MAX_WORK_YN
#   value: 10
#   desc: _YN 最多同时运行多少个
#
# - key: RUNNING_OPTIONS_YN
#   value: 30
#   desc: _YN 每次运行多少个任务
#
# - key: YN_TOKEN
#   value: ae809140c0424b3ac86783d470079c0
#   desc: _YN TOKEN
#
#
#
# - key: YN_PROJECTID
#   value: 9430
#   desc: YN_PROJECTID
#



# - key: username_FENG_YUN
# value: nkzg1023
# desc: 风云
# 登录账号
#
# - key: password_FENG_YUN
# value: 12346789
# desc: 风云
# 登录账号
#
# - key: API_PROXY_FENG_YUN
# value: "https://api.yunma.la"
# desc: 七彩API
# 获取手机号的方式
#
# - key: MAX_WORK_FENG_YUN
# value: 5
# desc: 七彩API
# 最多同时运行多少个
#
# - key: RUNNING_OPTIONS_FENG_YUN
# value: 8
# desc: 七彩API
# 每次运行生成多少个手机号

Proxy_Right = os.path.join(ROOT_PATH, "Proxy_Right.txt")
RightProxy_PATH = os.path.join(ROOT_PATH, "Proxy_Used.txt")
ErrProxy_PATH = os.path.join(ROOT_PATH, "Proxy_Error.txt")

with open(RightProxy_PATH, "r") as f:
    HTTP_PROXY_POOL_TXT = f.read()
with open(Proxy_Right, "r") as f:
    IP_Position = json.loads(f.read())
HTTP_PROXY_POOL = list(filter(None, HTTP_PROXY_POOL_TXT.split("\n")))
shuffle(HTTP_PROXY_POOL)
print("当前代理池有")
for line in HTTP_PROXY_POOL:
    print(line+'\n')
# 存放代理文件的路径
PROXY_DIR = os.path.join(ROOT_PATH, 'proxy')

LOG_DIR = os.path.join(ROOT_PATH, "logs")  # 主项目日志
DbFilePath = os.path.join(ROOT_PATH, "Nike.db")  # 数据库 存放路径 项目路径/Nike.db
os.makedirs(LOG_DIR, exist_ok=True)  # 创建日志文件， 如果有的话，不需要创建
os.makedirs(PROXY_DIR, exist_ok=True)  # 创建日志文件， 如果有的话，不需要创建


# coding=utf-8
import logging
import os
import threading
from logging.handlers import RotatingFileHandler

from utils.Constant import LOG_DIR

# 把发出日志的logger的名字也打印出来
fmt = logging.Formatter(
    '%(asctime)s [%(levelname)6s %(name)s] [%(thread)d %(threadName)s] [%(pathname)s:%(lineno)d:%(funcName)s]\n%(message)s')


def routeFileH(name, sizeM, count):
    _name = name.replace(os.sep, '_')
    rotateH = RotatingFileHandler(os.path.join(LOG_DIR, f'{_name}.log'), maxBytes=sizeM * 2 ** 20, backupCount=count)
    rotateH.setFormatter(fmt)
    rotateH.setLevel(logging.DEBUG)
    return rotateH


def streamH():
    _h = logging.StreamHandler()
    _h.setFormatter(fmt)
    _h.setLevel(logging.DEBUG)
    return _h


# 日志默认是有传播属性的,把stream handler放到root上面,那么所有的日志都会在terminal上出来
# 所有的日志也需要出现在all.log里面
# 有时候我们要在terminal里面看日志，这个时候不需要有太多东西了,不然看不到
def configRootLogger():
    _log = logging.getLogger()
    _log.addHandler(routeFileH('all', 200, 5))
    _log.setLevel(logging.DEBUG)


configRootLogger()


# 最多存250M
def configPocTestLogger(name, sizeM=5, count=50):
    _log = logging.getLogger(name)
    _log.addHandler(streamH())
    _log.addHandler(routeFileH(name, sizeM, count))
    _log.setLevel(logging.DEBUG)
    return _log


def getLogObj(name):
    return configPocTestLogger(name)


existsCaseLogger = set()

threadIdToCase = {}


def getCaseLogger(caseName):
    _log = logging.getLogger(f'{caseName}')
    threadIdToCase[threading.get_ident()] = caseName
    if caseName not in existsCaseLogger:
        existsCaseLogger.add(caseName)
        _log.addHandler(routeFileH(caseName, 2, 30))
        _log.addHandler(streamH())

    return _log


log = getLogObj
import time


def getLogName():
    first_date = "all_" + time.strftime('%Y_%m_%d_%H_%M_%S', time.localtime(time.time()))
    return first_date

import os
import zipfile
from random import shuffle

from utils.Constant import PROXY_DIR
from utils.fileUtils import render

# 存储自定义Chrome代理扩展文件的目录
CUSTOM_CHROME_PROXY_EXTENSIONS_DIR = os.path.join(PROXY_DIR, 'chrome-proxy-extensions')
from utils.Constant import HTTP_PROXY_POOL
from utils.Constant import API_PROXY_TYPE1, API_PROXY_TYPE2
import threading

lock = threading.Lock()
import queue

proxyQueue = queue.Queue()
proxyURL = queue.Queue()
import copy

list1 = copy.deepcopy(API_PROXY_TYPE1)

list1.append(API_PROXY_TYPE2)
lock2 = threading.Lock()


def getURL():
    with lock2:
        if proxyURL.empty():
            for line in list1:
                proxyURL.put(line)
        ip = proxyURL.get()

    return ip


def GetProxyFile():
    # 生成代理配置文件
    with lock:
        if proxyQueue.empty():
            for line in HTTP_PROXY_POOL:
                proxyQueue.put(line)
        proxyAddress = proxyQueue.get()
        data = get_chrome_proxy_extension(proxyAddress)
        assert os.path.exists(data), "代理文件不存在"
        return proxyAddress, data


def getProxy(HTTP_PROXY_POOL):
    httpProxy = [{"http": proxy} for proxy in HTTP_PROXY_POOL]
    shuffle(httpProxy)
    return httpProxy


def GetProxyFileALL(API_PROXYP_LIST):
    # 生成代理配置文件
    with lock:
        API_PROXYP_LIST = list(API_PROXYP_LIST)
        shuffle(API_PROXYP_LIST)
        [get_chrome_proxy_extension(IP) for IP in API_PROXYP_LIST]


def get_chrome_proxy_extension(proxy):
    """
    获取一个Chrome代理扩展,里面配置有指定的代理(带用户名密码认证)
    proxy - 指定的代理,格式: username:password@ip:port
    """
    # "42.101.9.225:4848:cgtod1666711398:apu0g7"
    proxylist = proxy.split(":")
    ip = proxylist[0]
    port = proxylist[1]
    username = proxylist[2]
    password = proxylist[3]
    proxyType = "http"
    if proxy:
        # 提取代理的各项参数
        # 创建一个定制Chrome代理扩展(zip文件)
        if not os.path.exists(CUSTOM_CHROME_PROXY_EXTENSIONS_DIR):
            os.mkdir(CUSTOM_CHROME_PROXY_EXTENSIONS_DIR)

        file = "_".join(proxylist).replace(".", "_").replace(":", "_")
        extension_file_path = os.path.join(CUSTOM_CHROME_PROXY_EXTENSIONS_DIR, f'{proxyType}_{file}.zip')
        # extension_file_path = f"{proxyType}_{file}.zip"
        if not os.path.exists(extension_file_path):
            # 扩展文件不存在，创建
            zf = zipfile.ZipFile(extension_file_path, mode='w')

            with open(os.path.join(PROXY_DIR, 'manifest.json'), "r") as f:
                manifest = f.read()

            zf.writestr('manifest.json', manifest)
            backgroundFile = os.path.join(PROXY_DIR, "background.js.template")
            backgroundFileValue = render(backgroundFile, proxyType=proxyType, username=username, password=password,
                                         ip=ip, port=port)

            zf.writestr("background.js", backgroundFileValue)
            zf.close()
        assert os.path.exists(extension_file_path), "代理文件不存在"
        return extension_file_path
    else:
        raise Exception('Invalid proxy format. Should be username:password@ip:port')

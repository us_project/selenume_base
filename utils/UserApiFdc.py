import copy
# 设计思路
# 每一个接口都有可能在访问的时候突然断掉， 我们假设会有这种情况，
# 由于他们的数据库都是同一个，所以在下面的执行过程中我们动态切换代理，
# 不让他们退出，在获取手机号的时候， 有一个超时时间控制 默认120 s , 可以修改配置文件更改
import random
import time
from random import shuffle

import requests
from bs4 import BeautifulSoup

from autoTest.metaClass.atomic_func import WebDriver
from model import crudOperate
from utils.Constant import API_PROXY_FENG_YUN
from utils.Constant import API_PROXY_QICAI, ITEMID_QICAI, username_QICAI, password_QICAI
from utils.Constant import API_PROXY_TYPE2
from utils.Constant import API_PROXY_YN
from utils.Constant import HTTP_PROXY_POOL
from utils.Constant import ITEMID, API_PROXY_TYPE1, TIMEOUT, PHONETYPE
from utils.Constant import ITEMID_type2
from utils.Constant import YN_PROJECTID
from utils.Constant import YN_TOKEN
# from utils.Constant import PROXY_FENGYUN
from utils.Constant import username, password
from utils.Constant import username_type2, password_type2
from utils.ConstantEnum import ComStatus, CRUD_ENUM
from utils.LogConfig import getCaseLogger
from utils.ProxyUtils import getProxy
from utils.custumException import *


def getNumber(num):
    a = "".join(list(filter(str.isdigit, num)))
    return a

class GetProxyPhone():
    def __init__(self):
        self.proxyIP = getProxy(HTTP_PROXY_POOL)
        print(f"您当前获取到的代理IP 地址是 {self.proxyIP}")
        self.proxies = None
        self.phone = None

    def parseProxy(self, ip):
        # 185.230.249.218:63938:leaf:vMocmJoc
        proxyList = ip["http"].split(":")
        self.proxies = {"http": f"http://{proxyList[2]}:{proxyList[3]}@{proxyList[0]}:{proxyList[1]}"}
        print(f"使用代理{self.proxies}")
        return self.proxies

    def getRandomProxy(self):
        ip = random.choice(self.proxyIP)
        return self.parseProxy(ip)

    def InsertData(self, params):
        assert self.phone, "当前没有获取到手机号码,程序退出"
        params["phonenumber"] = self.phone
        crudOperate(CRUD_ENUM.INSERT.value, params)  # 入库

    def updateStatus(self, params):
        assert self.phone, "当前没有获取到手机号码,程序退出"
        params["phonenumber"] = self.phone
        crudOperate(CRUD_ENUM.UPDATE.value, params)
    def deleteData(self, params):
        assert self.phone, "当前没有获取到手机号码,程序退出"
        params["phonenumber"] = self.phone
        crudOperate(CRUD_ENUM.DELETE.value, params)


class GetPhoneCode(WebDriver, GetProxyPhone):
    def __init__(self):
        GetProxyPhone.__init__(self)

        self.headers = None
        self.phone = None
        self.comid = None
        self.authtoken = None
        self.baseAPI = None
        self.driver = None
        self.code = None
        self.msgPre = ""
        self.proxies = None
        self.seDriver = None
        self.log = None

    def login(self):
        """
            动态获取代理URL ， 返回一个 可以使用的url ， 执行时间 约为 0.4s 尝试100次（默认）
        """
        baseAPI = copy.deepcopy(API_PROXY_TYPE1)
        shuffle(baseAPI)
        self.msgPre += "现在开始获取代理IP"
        self.msgPre += "现在开始用户登录"
        print("现在开始获取代理IP")
        print("现在开始用户登录")

        headers = {}
        params = {
            'account': username,
            'password': password,
            'loginsource': "API",
        }
        for ip in self.proxyIP:
            self.parseProxy(ip)
            for api in baseAPI:
                try:
                    url = api + "/api" + "/login"
                    print(f"开始访问url {url} 代理: {self.proxies}")
                    login = requests.get(url, params=params, timeout=3, proxies=self.proxies).json()
                    if login['code'] == 1:
                        # 里面还有其他信息 这里只拿token
                        # 直接将token放在全局里面
                        authtoken_ = login['data']['authtoken']
                        headers['Authorization'] = authtoken_
                        self.headers = headers
                        self.authtoken = authtoken_
                        self.baseAPI = api + "/api"
                        self.msgPre += f"url :{url} 用户{username} 登录成功， authtoken:{authtoken_} ,当前headers :{headers}"
                        print(f"用户{username} 登录成功， authtoken:{authtoken_} ,当前headers :{headers}")
                        return
                except requests.exceptions.RequestException as e:
                    msg = f"{self.proxies} 访问API失败,尝试换另一种"
                    print(msg)
                    self.msgPre += msg
                    continue
                except Exception as e:
                    msg = f"{self.proxies} 访问API失败,尝试换另一种"
                    print(msg)
                    self.msgPre += msg
                    continue

        raise ProxyError("代理池访问不通 错误")

    # def loadUserinfo(self):
    #     '''
    #     获取当前登录的用户信息
    #     :return:
    #     '''
    #     print(f"开始获取用户信息")
    #     self.msgPre += "开始获取用户信息"
    #     loadUserinfo = requests.get(self.baseAPI + "/user/loadUserinfo", headers=self.headers, timeout=3,
    #                                 proxies=self.proxies).json()
    #     if loadUserinfo['code'] == 1:
    #         print(f"当前用户信息为：{loadUserinfo}")
    #         print(loadUserinfo)
    #     else:
    #         print("获取用户信息失败")
    #         return False
    #
    # def loadUserWallet(self):
    #     log.info("开始获取用户余额")
    #     loadUserWallet = requests.get(self.baseAPI + "/user/loadUserWallet", headers=self.headers, timeout=3,
    #                                   proxies=self.proxies).json()
    #     if loadUserWallet['code'] == 1:
    #         yue = loadUserWallet["data"]["itemlist"][0]["limit"]
    #         assert yue > 0, "当前余额不足，请及时充值..."
    #         log.info(f"当前用户余额为：{yue}元 返回值是：{loadUserWallet}")
    #     else:
    #         log.info(f"获取用户余额信息失败")
    #         return False

    def getPhone(self, itemid=ITEMID, phone="", phonetype=PHONETYPE, cnt=3):
        '''
        :param itemid:  你项目的itemid
        :param phone:  如果需要指定手机号 那就就填写
        :param phonetype: 0随机，1实体号，2虚拟号
        :param cnt: 超时时间
        :return:
        '''
        self.msgPre += "开始获取手机号码"
        for ip in self.proxyIP:
            self.parseProxy(ip)
            try:
                params = {
                    'itemid': itemid,
                    'phonetype': phonetype,
                    'form': "API",
                    "phone": phone
                }

                loadReceivePhone = requests.get(self.baseAPI + "/phone/loadReceivePhone", params=params,
                                                headers=self.headers, timeout=3, proxies=self.proxies).json()
                if loadReceivePhone['code'] == 1:
                    res_date = loadReceivePhone['data']
                    phoneData = {"phone": res_date['itemlist']['phone'], "comid": res_date['comid']}
                    self.comid = res_date['comid']
                    self.phone = res_date['itemlist']['phone']
                    self.log = getCaseLogger(self.phone)
                    self.log.info(f"获取手机号码成功，phoneData：{phoneData} API: {self.phone}")
                    return

                else:
                    msg = f"获取手机号码失败， 响应内容：{loadReceivePhone} "
                    msg += f"现在开始换另一个代理"
                    self.log.info(msg)
                    self.login()
                    raise ProxyError(msg)
            except requests.exceptions.RequestException as e:
                msg = "访问API失败,尝试换另一种"
                self.log.info(msg)
                continue
            except Exception as e:
                msg = "访问API失败,尝试换另一种"
                self.log.info(msg)
                continue

    def getCode(self, cnt=TIMEOUT):
        """
                '''
        :param cnt 超时时间
        :return:
        '''
        """
        startTime = time.time()

        def isOverTime():
            if 180 < (time.time() - startTime):
                self.log.info("验证码超时")
                raise getCodeTimeError("验证码超时")

        self.log.info("开始请求验证码")
        while True:
            for ip in self.proxyIP:
                self.parseProxy(ip)
                while cnt > 0:
                    try:
                        json_data = {
                            'comid': self.comid,
                        }
                        response = requests.get(self.baseAPI + '/phone/getPutSmsResult', headers=self.headers,
                                                params=json_data, timeout=3, proxies=self.proxies)
                        json_resp = response.json()
                        self.log.info(f"响应成功，返回值：{json_resp}")
                        if json_resp['code'] == 1:
                            comdata_ = json_resp['data']['comdata']
                            status_ = comdata_['com_status']
                            self.log.info(f"当前 {self.phone} com_status 返回码为{status_} com_code:{comdata_['com_code']}")
                            if status_ == ComStatus.AlreadyBackCode.value:
                                code = comdata_['com_code']
                                self.code = code
                                self.log.info(f"已成功接收到手机验证码，验证码为{code}")
                                return
                            else:
                                self.log.info(f"{self.phone} 暂未收到手机验证码, 继续等待")
                                cnt -= 1
                                time.sleep(1)
                        isOverTime()
                    except requests.exceptions.RequestException as e:
                        msg = "访问API失败,尝试换另一种"
                        self.log.info(msg)
                        time.sleep(1)
                        isOverTime()
                        break

                    except Exception as e:
                        # 换代理
                        msg = "访问API失败,尝试换另一种"
                        self.log.info(msg)
                        time.sleep(1)
                        isOverTime()
                        break
                    isOverTime()

    def releasePhone(self, cnt=3):
        self.log.info("现在开始释放手机号")
        if not self.phone:
            self.log.info("没有获取到手机号， 不需要释放")
        while cnt > 0:
            try:
                params = {
                    'itemid': ITEMID,
                    'phone': "{}".format(self.phone),
                }
                response = requests.get(self.baseAPI + '/phone/submitRelasePhone', headers=self.headers,
                                        params=params, timeout=2, proxies=self.proxies)
                json_resp = response.json()
                if json_resp['code'] == 1:
                    self.log.info("在{}项目中释放手机号{}成功 响应值：{}".format(ITEMID, self.phone, json_resp))
                    break
                else:
                    self.log.info("释放手机号失败，正在重试")
                    cnt -= 1

            except Exception as _:
                # self.login()
                time.sleep(1)
                cnt -= 1
        self.updateStatus({"release": "1"})
        assert cnt > 0, "释放手机号发生了错误，请稍后重试"

    def startChrom(self):
        # 调用谷歌浏览器
        WebDriver.__init__(self, self.phone, self.log)
        self.startChromDriver()


class GetYN(WebDriver, GetProxyPhone):
    def __init__(self, phone=None):
        GetProxyPhone.__init__(self)
        self.msgPre = ""
        self.baseAPI = API_PROXY_YN
        self.token = YN_TOKEN
        self.phone = phone
        self.code = None
        self.seDriver = None
        self.log = None
        self.password = None
        self.projectId = YN_PROJECTID

    def startChrom(self):
        # 调用谷歌浏览器
        WebDriver.__init__(self, self.phone, self.log)
        self.startChromDriver()

    # 1
    # ae809140c0424b3ac86783d470079c0
    # 项目ID
    # 9430 1ae809140c0424b3ac86783d470079c0 项目ID 9430
    def getPhone(self):
        if self.phone:
            self.log = getCaseLogger(self.phone)
            self.log.info(f"获取手机号 {self.phone} 成功 , 这个是已经失败的,重新拉起来")
            return
        print("Now is getPhone")
        api = f"""{self.baseAPI}/api/getPhone?token={self.token}&projectId={self.projectId}"""
        while True:
            response = requests.get(api).json()
            if str(response["code"]).strip() == "25200":
                self.phone = response["data"]
                self.log = getCaseLogger(self.phone)
                self.log.info(f"获取手机号 {self.phone} 成功 API: {api}")
                return
            else:
                print(f"not get Phone , later...{response=}")
            time.sleep(0.2)

    def getCode(self, cnt=TIMEOUT):
        self.log.info("Start get Code ")

        def getNumber(num):
            a = "".join(list(filter(str.isdigit, num)))
            return a

        api = f"""{self.baseAPI}/api/getSms?token={self.token}&projectId={self.projectId}&phone={self.phone}"""
        startTime = time.time()

        def isOverTime():
            if 180 < (time.time() - startTime):
                self.log.info("验证码超时")
                raise getCodeTimeError("验证码超时")

        while True:
            response = requests.get(api).json()
            if str(response["code"]).strip() == "25200":
                self.code = getNumber(response["data"])
                self.log.info(f"getCode is Success: {self.code}")
                return
            else:
                self.log.info(f"getCode is error API:{api}")
                time.sleep(0.5)
            isOverTime()

    def release(self):
        if self.phone:
            api = f"""{self.baseAPI}/api/offline2"""
            response = requests.post(api, data={"projectId": self.projectId, "token": self.token,
                                                "phone": self.phone}).json()
            if str(response["code"]).strip() == "25200":
                self.log.info("接口拉黑成功")
        else:
            print("当前任务没有取到号码,不需要拉黑")


class GetPhoneCode2(WebDriver, GetProxyPhone):
    def __init__(self, phone=None):
        GetProxyPhone.__init__(self)
        self.msgPre = ""
        self.baseAPI = API_PROXY_TYPE2
        self.token = None
        self.phone = phone
        self.code = None
        self.seDriver = None
        self.log = None
        self.password = None

    def login(self):
        # {"code": 200, "msg": "", "result": {"token": "VUpqQll4bnZObkJzZ3BCMW9XMUI5Z1pYZFdSOGhQNk5iVHBzRmFZWFZiND0="}}
        api = f"""{self.baseAPI}/api/user/apiLogin?username={username_type2}&password={password_type2}"""
        while True:
            msg = f"开始登陆{api}  "
            print(msg)
            response = requests.get(api).json()
            print(f"登陆响应: {response}")
            if response["code"] == 200:
                msg = f"访问API{api} 登陆成功 "
                print(api)
                self.msgPre += msg
                self.token = response["result"]["token"]
                return
            time.sleep(0.5)

    def getPhone(self):
        if self.phone:
            self.log = getCaseLogger(self.phone)
            self.log.info(f"获取手机号 {self.phone} 成功 , 这个是已经失败的,重新拉起来")
            return
        # {"code":200,"message":"成功":"result":{"phones":"19999999999"}}
        print("Now is getPhone")
        api = f"""{self.baseAPI}/api/phone/getPhone?productId={ITEMID_type2}&token={self.token}"""
        while True:
            response = requests.get(api).json()
            if response["code"] == 200:
                self.phone = response["result"]["phones"]
                self.log = getCaseLogger(self.phone)
                self.log.info(f"获取手机号 {self.phone} 成功 API: {api}")
                return
            else:
                print(f"not get Phone , later...{response=}")
            time.sleep(0.2)

    def getCode(self, cnt=TIMEOUT):
        self.log.info("Start get Code ")
        # 返回示例：{"code":200,"message":"成功":"result":{"status":1,"code":"111111"}}
        api = f"""{self.baseAPI}/api/phone/getCode?productId={ITEMID_type2}&token={self.token}&phone={self.phone}"""
        startTime = time.time()

        def isOverTime():
            if 180 < (time.time() - startTime):
                self.log.info("验证码超时")
                raise getCodeTimeError("验证码超时")

        while True:
            response = requests.get(api).json()
            if response["code"] == 200 and response["result"]["status"] == 1:
                self.code = response["result"]["code"]
                self.log.info(f"getCode is Success: {self.code}")
                return
            else:
                self.log.info(f"getCode is error API:{api}")
                time.sleep(0.5)
            isOverTime()

    def reportResult(self, status, cnt=5):
        """
            上报接口
            # 结果，1-注册成功，2-无验证码，3-已注册，4-注册失败，5-账户被封禁

        """
        api = f"""{self.baseAPI}/api/phone/reportResult?phone={self.phone}&result={status}&productId={ITEMID_type2}&token={self.token}"""
        while True:
            response = requests.get(api).json()
            if response["code"] == 200:
                self.log.info("reportResult is Success")
                return
            else:
                self.log.info(f"reportResult is Error API:{api}")

    def startChrom(self):
        # 调用谷歌浏览器
        WebDriver.__init__(self, self.phone, self.log)
        self.startChromDriver()


class GetPhoneCodeQicai(WebDriver, GetProxyPhone):
    def __init__(self, phone=None):
        GetProxyPhone.__init__(self)
        self.msgPre = ""
        self.baseAPI = API_PROXY_QICAI
        self.token = None
        self.phone = phone
        self.code = None
        self.seDriver = None
        self.log = None
        self.tradeNo = None
        self.password = None

    def getMoney(self):
        api = f"""{self.baseAPI}/v2/user/balance?username={username_QICAI}&password={password_QICAI}"""
        response = requests.get(api, headers={"Content-Type": "multipart/form-data"}).json()
        if response["code"] == 0:
            print(f"您当前的余额是 {response['message']}")

    def getPhone(self):
        print("Now is getPhone")
        api = f"""{self.baseAPI}/v2/phone/get?username={username_QICAI}&password={password_QICAI}&entryid={ITEMID_QICAI}"""
        while True:
            print("get Code Start ")
            response = requests.get(api, headers={"Content-Type": "multipart/form-data"}).json()
            if response["code"] == 0:
                self.phone = response["result"]["phone"]
                self.tradeNo = response["result"]["tradeNo"]
                self.log = getCaseLogger(self.phone)
                self.log.info(f"获取手机号 {self.phone} 成功 API: {api}")
                return
            else:
                print(f"not get Phone , later...{api=} {response=}")

    def getCode(self, cnt=TIMEOUT):
        self.code = "456645612"
        self.log.info("Start get Code ")
        # 返回示例：{"code":200,"message":"成功":"result":{"status":1,"code":"111111"}}
        api = f"""{self.baseAPI}/v2/phone/code?username={username_QICAI}&password={password_QICAI}&trade_no={self.tradeNo}"""
        startTime = time.time()

        def isOverTime():
            if 300 < (time.time() - startTime):
                self.log.info("5分钟 验证码超时")
                raise getCodeTimeError("5分钟 验证码超时")

        while True:
            response = requests.get(api).json()
            if response["code"] == 0:
                self.code = response["result"]["code"]
                self.log.info(f"getCode is Success: {self.code}")
                return
            else:
                self.log.info(f"getCode is error {api=} {response=}")
                time.sleep(0.5)
            isOverTime()

    def reportResult(self, status=4):
        """
            上报接口
            # 结果，1-注册成功，2-无验证码，3-已注册，4-注册失败，5-账户被封禁
        """
        # 反馈值  1取码成功，2无验证码，3已注册 4其它
        api = f"""{self.baseAPI}/v2/phone/code/feed?username={username_QICAI}&password={password_QICAI}&trade_no={self.tradeNo}&status={status}"""
        response = requests.get(api).json()
        self.log.info(f"上报结果 {api=} {response=}")

    def startChrom(self):
        # 调用谷歌浏览器
        WebDriver.__init__(self, self.phone, self.log)
        self.startChromDriver()


class Get_2023(WebDriver, GetProxyPhone):
    def __init__(self, phone=None):
        GetProxyPhone.__init__(self)
        self.msgPre = ""
        self.baseAPI = "http://118.195.190.73:8061"
        self.token = None
        self.phone = phone
        self.code = None
        self.seDriver = None
        self.log = None
        self.tradeNo = None
        self.password = None
        self.paramsStr = "?token=5b555396ae1f8e5accc768100b2f90e6&projectID=35412581"

    def getPhone(self):
        print("Now is getPhone")
        api = f"""{self.baseAPI}/api/getPhone{self.paramsStr}"""
        while True:
            print("get Code Start ")
            response = requests.get(api, headers={"Content-Type": "multipart/form-data"}).json()
            if response["code"] == "10000":
                self.phone = response["data"]["phone"]
                self.log = getCaseLogger(self.phone)
                self.log.info(f"获取手机号 {self.phone} 成功 API: {api}")
                return
            else:
                print(f"not get Phone , later...{api=} {response=}")

    def getCode(self, cnt=TIMEOUT):
        self.code = "456645612"
        self.log.info("Start get Code ")
        # 返回示例：{"code":200,"message":"成功":"result":{"status":1,"code":"111111"}}
        api = f"""{self.baseAPI}/api/getPhoneSms{self.paramsStr}&phone={self.phone}"""

        startTime = time.time()

        def isOverTime():
            if 300 < (time.time() - startTime):
                self.log.info("5分钟 验证码超时")
                raise getCodeTimeError("5分钟 验证码超时")

        while True:
            response = requests.get(api).json()
            if response["code"] == "10000":
                self.code = response["data"]["sms"]
                self.log.info(f"getCode is Success: {self.code}")
                return
            else:
                self.log.info(f"getCode is error {api=} {response=}")
                time.sleep(0.5)
            isOverTime()

    def startChrom(self):
        # 调用谷歌浏览器
        WebDriver.__init__(self, self.phone, self.log)
        self.startChromDriver()


class GetPhoneCode_Fengyun(WebDriver, GetProxyPhone):
    def __init__(self, phone=None, token=None):
        GetProxyPhone.__init__(self)
        self.msgPre = ""
        self.baseAPI = API_PROXY_FENG_YUN
        self.token = token
        self.phone = phone
        self.code = None
        self.seDriver = None
        self.log = None
        self.tradeNo = None
        self.password = None
        self.API_NAME = "风云API"
        self.orderId = ""
        print("已传递token,无需登录")
        self.user_agent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36"

    # def login(self):
    #         # https://api.yunma.la
    #         # {"code":0,"message":"获取token成功","data":"20c938c3-b8e7-4150-8fa2-e279f5fe12b3"}
    #         api = f"""{self.baseAPI}/app/getLogin?username={username_FENG_YUN}&password={password_FENG_YUN}"""
    #         print(f"{self.API_NAME} Login Start {api=}")
    #         response = requests.get(api).json()
    #         if response["code"] == 0:
    #             self.token = response["data"]
    #             print(f"登陆成功,{response}")
    #         else:
    #             raise Exception("登陆失败 , 请检查账号密码 ")

    # def requests(self, url):
    #     global response
    #     while True:
    #         try:
    #             if PROXY_FENGYUN:
    #                 response = requests.get(url, timeout=60, proxies=self.proxies).json()
    #             else:
    #                 response = requests.get(url, timeout=60).json()
    #         except requests.exceptions.ConnectTimeout as e:
    #             print("超时异常 获取手机号 requests.exceptions.ConnectTimeout , 正在尝试重试 获取手机号")
    #             time.sleep(180)
    #         except requests.exceptions.ConnectionError as e:
    #             print("超时异常 获取手机号 requests.exceptions.ConnectionError, 正在尝试重试 获取手机号")
    #             time.sleep(180)
    #         except Exception as e:
    #             print("超时异常 获取手机号 Exception, 正在尝试重试 获取手机号")
    #             time.sleep(180)
    #
    #         return response

    def getPhone(self):
        print(f"{self.API_NAME} Now is getPhone")
        api = f"""{self.baseAPI}/app/sms/getPhone?token={self.token}"""
        while True:
            try:
                proxies = self.getRandomProxy()
                proxies["https"] = proxies["http"]
                print(f"{self.API_NAME} 获取手机号 {api=} {proxies=}")
                head = {
                    'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36',
                    'Connection': 'keep-alive'}
                response = requests.get(api, headers=head, proxies=proxies)
                response = response.json()
                if response["code"] == 0:
                    phone = response["data"]["phone"]
                    self.phone = response["data"]["phone"]
                    self.orderId = response["data"]["orderId"]
                    self.log = getCaseLogger(self.phone)
                    self.log.info(f"获取手机号 {self.phone} 成功 API: {api}, {response}")

                    return
                else:
                    print(f"{self.API_NAME} not get Phone , later...{api=} {response=}")
            except requests.exceptions.ConnectTimeout as e:
                print("超时异常 获取手机号 requests.exceptions.ConnectTimeout , 正在尝试重试 获取手机号 此刻休眠 5s")
                time.sleep(5)
            except requests.exceptions.ConnectionError as e:
                print("超时异常 获取手机号 requests.exceptions.ConnectionError, 正在尝试重试 获取手机号 此刻休眠 5s")
                time.sleep(5)
            else:
                time.sleep(2)

    def getCode(self):
        self.log.info("Start get Code ")
        # 返回示例：{"code":200,"message":"成功":"result":{"status":1,"code":"111111"}}
        api = f"""{self.baseAPI}/app/sms/getSmsCode?token={self.token}&orderId={self.orderId}"""
        startTime = time.time()
        waitTime = 10

        def isOverTime():
            if 200 < (time.time() - startTime):
                self.log.info("180s 验证码超时")
                raise getCodeTimeError("180s 验证码超时")

        while True:
            try:
                response = requests.get(api, proxies=self.getRandomProxy()).json()
                if response["code"] == 0:
                    # {"code":0,"message":"验证码查询成功","data":"234532"}
                    self.code = response["data"]
                    self.log.info(f"getCode is Success: {self.code}")
                    return
                else:
                    self.log.info(f"getCode is error {api=} {response=}")
                    time.sleep(0.5)
                isOverTime()
            except requests.exceptions.ConnectTimeout as e:
                print("超时异常 获取验证码 requsets.exceptions.ConnectTimeout , 等待 5s 后在开始获取验证码")
                time.sleep(waitTime)
            except requests.exceptions.ConnectionError as e:
                print("超时异常 获取验证码 requests.exceptions.ConnectionError , 等待 5s 后在开始获取验证码")
                time.sleep(waitTime)
            except getCodeTimeError as e:
                raise getCodeTimeError("180s 验证码超时")

            except Exception as e:
                print("超时异常 获取验证码 requests.exceptions.ConnectionError , 等待 5s 后在开始获取验证码")
                time.sleep(waitTime)
            else:
                time.sleep(waitTime)
            finally:
                isOverTime()

    def reportResult(self, status=None):
        """
            上报接口
            # 结果，1-注册成功，2-无验证码，3-已注册，4-注册失败，5-账户被封禁
        """
        # 反馈值  1取码成功，2无验证码，3已注册 4其它
        if not self.phone:
            return
        api = f"""{self.baseAPI}/app/sms/feedback?token={self.token}&orderId={self.orderId}&status={status}"""
        response = requests.get(api, proxies=self.getRandomProxy()).json()
        self.log.info(f"上报结果 {api=} {response=}")

    def startChrom(self):
        # 调用谷歌浏览器
        WebDriver.__init__(self, self.phone, self.log)
        self.startChromDriver()


class GetSIFANG(WebDriver, GetProxyPhone):
    def __init__(self, phone=None):
        GetProxyPhone.__init__(self)
        self.msgPre = ""
        self.phone = phone
        self.code = None
        self.seDriver = None
        self.log = None
        self.password = None
        self.log = getCaseLogger(phone)

    def startChrom(self):
        # 调用谷歌浏览器
        WebDriver.__init__(self, self.phone, self.log)
        self.startChromDriver()

    def getCode(self, cnt=TIMEOUT):
        self.log.info("Start get Code ")
        startTime = time.time()

        def isOverTime():
            if 180 < (time.time() - startTime):
                self.log.info("验证码超时")
                raise getCodeTimeError("验证码超时")
        while True:
            codePage = requests.get("http://43.159.63.248/?form=zhongopk",proxies=self.getRandomProxy(),timeout=30)
            self.log.info("开始获取验证码 ")
            try:
                if codePage.status_code == 200:
                    soup = BeautifulSoup(codePage.text, 'html.parser')
                    for link in soup.find_all('tr'):
                        if "来自" not in link.text:
                            tdList = link.find_all('td')
                            if self.phone[:3]+"****"+self.phone[7:] == tdList[1].text and "【NIKE中国】" in tdList[2].text:
                                self.code = getNumber(tdList[2].text)
                                self.log.info(f"手机号验证码获取成功 {self.phone} : {self.code}")
                                return
                elif codePage.status_code == 502:
                    raise API_NOT_RESPONSE("API 网站蹦了,程序退出")
            except requests.exceptions.ConnectTimeout as e:
                raise API_NOT_RESPONSE("API 网站蹦了,程序退出")
            except requests.exceptions.ConnectionError as e:
                raise API_NOT_RESPONSE("API 网站蹦了,程序退出")

            finally:
                time.sleep(3)
                isOverTime()


    def getCodeSzfangmm(self, cnt=TIMEOUT):
        self.log.info("Start get Code ")
        startTime = time.time()

        def isOverTime():
            if 180 < (time.time() - startTime):
                self.log.info("验证码超时")
                raise getCodeTimeError("验证码超时")
        while True:
            codePage = requests.get("http://sms.szfangmm.com:3000/xR5s8hNK6zFAz4xTpkzEXo",timeout=30)
            self.log.info("接口获取验证码请求成功")
            try:
                if codePage.status_code == 200:
                    soup = BeautifulSoup(codePage.text, 'html.parser')
                    for link in soup.find_all('tr'):
                        if "發送號碼" not in link.text:
                            tdList = link.find_all('td')
                            if self.phone[:3]+"****"+self.phone[7:] == tdList[1].text and "【NIKE中国】" in tdList[2].text:
                                self.code = getNumber(tdList[2].text)
                                self.log.info(f"手机号验证码获取成功 {self.phone} : {self.code}")
            except Exception as e:
                self.log.info(f"手机号验证码获取失败 {self.phone} 正在重试 {str(e)}")
            finally:
                time.sleep(3)
                isOverTime()

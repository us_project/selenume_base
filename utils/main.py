import time

import requests

username = "luege123"
password = "zl56226162"

headers = {

}

baseAPI = "http://api.yjyfdc.com/api"


def login():
    '''
        登录
        :return:
        '''
    params = {
        'account': username,
        'password': password,
        'loginsource': "API",
    }
    login = requests.get(baseAPI + "/login", params=params).json()
    if (login['code'] == 1):
        # 里面还有其他信息 这里只拿token
        # 直接将token放在全局里面
        authtoken_ = login['data']['authtoken']
        headers['Authorization'] = authtoken_
        return authtoken_
    else:
        print("登录失败")
        return False


def loadUserinfo():
    '''
    获取当前登录的用户信息
    :return:
    '''
    loadUserinfo = requests.get(baseAPI + "/user/loadUserinfo", headers=headers).json()
    if (loadUserinfo['code'] == 1):
        print(loadUserinfo)
    else:
        print("获取用户信息失败")
        return False


def loadUserWallet():
    loadUserWallet = requests.get(baseAPI + "/user/loadUserWallet", headers=headers).json()
    if (loadUserWallet['code'] == 1):
        print(loadUserWallet)
    else:
        print("获取余额失败")
        return False


def getPhone(itemid, phone="", phonetype=0):
    '''
    :param itemid:  你项目的itemid
    :param phone:  如果需要指定手机号 那就就填写
    :param phonetype: 0随机，1实体号，2虚拟号
    :return:
    '''
    params = {
        'itemid': itemid,
        'phonetype': phonetype,
        'form': "API",
        "phone": phone
    }

    loadReceivePhone = requests.get(baseAPI + "/phone/loadReceivePhone", params=params, headers=headers).json()
    if (loadReceivePhone['code'] == 1):
        res_date = loadReceivePhone['data']
        return {"phone": res_date['itemlist']['phone'], "comid": res_date['comid']}
    else:
        print(loadReceivePhone)
        return False


def getPutSmsResult(comid):
    '''
    :param comid getPhone 接口返回的comid:
    :return:
    '''

    json_data = {
        'comid': comid,
    }

    response = requests.get(baseAPI + '/phone/getPutSmsResult', headers=headers,
                            params=json_data)
    json_resp = response.json()
    print(json_resp)
    if (json_resp['code'] == 1):
        comdata_ = json_resp['data']['comdata']
        status_ = comdata_['com_status']
        if (status_ == 1):
            code_ = comdata_['com_code']
            print("接到手机码{}".format(code_))
            return code_
        else:
            print("暂未收到")
            return False
    else:
        print("收取验证码出现异常")


def submitRelasePhone(itemid, phone):
    params = {
        'itemid': itemid,
        'phone': "{}".format(phone),
    }
    response = requests.get('http://yjyfdc.com/api/phone/submitRelasePhone', headers=headers,
                            params=params)
    json_resp = response.json()
    print(json_resp)
    if (json_resp['code'] == 1):
        print("在{}项目中释放手机号{}".format(itemid, phone))
        return True
    else:
        return False


def submitBlockPhone(itemid, phone):
    json_data = {
        'itemlist': [
            {
                'itemid': itemid,
                'phone': phone,
            },
        ],
        'remark': '使用了',
    }

    response = requests.post('http://yjyfdc.com/api/phone/submitBlockPhone', headers=headers,
                             json=json_data, verify=False)
    # 我的业务不需要拉黑 自己提起数据
    print("拉黑,{}".format(phone))


if __name__ == '__main__':
    # 需要调用一下登录接口 其实不用每次都调用 因为这个token长期有效 可以调用一次就把那个token放在请求头中 下次不用调用了
    itemid = 8012
    login()
    loadUserWallet()
    phone = getPhone(itemid)
    print("获取的手机号为{},id为{}".format(phone['phone'], phone["comid"]))
    if phone:
        # 这里对手机号进行业务处理
        # 用来接受时间来延迟判断是否成功
        time.sleep(5)
        while 1:
            result = getPutSmsResult(phone["comid"])
            if result:
                print(result)
                submitRelasePhone(itemid, phone['phone'])
                break
            time.sleep(2)


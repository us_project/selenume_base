from enum import Enum, unique


@unique
class ComStatus(Enum):
    """
    # "com_status": 1, //{"已提取", "已回码", "已完成", "已失败"}，
    # com_status返回的结果，是对应右边的数组下标，0 对应已提取，以此类推
    """
    AlreadyExtracted = 0
    AlreadyBackCode = 1
    COMPLETED = 2
    ERROR = 3


@unique
class CRUD_ENUM(Enum):
    INSERT = "INSERT"
    UPDATE = "UPDATE"
    SELECT = "SELECT"
    SELECTSINGLE = "SELECTSINGLE"
    DELETE = "DELETE"

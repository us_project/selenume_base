import random
import secrets
import string


def getRamdomStr():
    return secrets.choice(string.ascii_letters)


def getPassword():
    pre = [str(getRamdomStr()).upper() for line in range(random.randint(3, 6))]
    middle = [str(getRamdomStr()).lower() for line in range(random.randint(4, 6))]
    number = [str(random.randint(1, 9)) for line in range(random.randint(3, 6))]
    pre.extend(number)
    pre.extend(middle)
    random.shuffle(pre)
    return "".join(pre)


if __name__ == '__main__':
    aa = getPassword()
    print(aa, len(aa))

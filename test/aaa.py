import copy
import os
import random
import threading
import time

import yaml
from selenium import webdriver
from selenium.common.exceptions import StaleElementReferenceException
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait

from utils.Constant import CHROME_OPTIONS
from utils.Constant import IP_Position
from utils.Constant import ISUPDATE_PASSWORD
from utils.Constant import LOG_DIR
from utils.Constant import PASSWORD
from utils.Constant import ROOT_PATH
from utils.Constant import URL
from utils.ProxyUtils import GetProxyFile
from utils.UA import getSingleUA
from utils.custumException import *
from utils.fileUtils import renderStr
from utils.birth import getRandomBirth
locak = threading.Lock()


class WebDriver:
    def startChromDriver(self):
        chromeOptions = webdriver.ChromeOptions()
        if not CHROME_OPTIONS:
            chromeOptions.add_argument("--headless")
        self.seDriver = webdriver.Chrome(options=chromeOptions)
        self.seDriver.get("https://www.baidu.com")
        errXpath = "(//div[contains(.,'验证码无效')])[last()]"

        try:
            self.seDriver.find_element(By.XPATH, errXpath)
        except Exception as e :
            pass
        else:
            raise CODEInvalidException("当前验证码无效")
        print("sdf")

if __name__ == '__main__':
    WebDriver().startChromDriver()
import sys
from concurrent.futures import ThreadPoolExecutor

from utils.Constant import MAX_WORK_FENG_YUN
from utils.Constant import RUNNING_OPTIONS_FENG_YUN
from utils.Constant import ROOT_PATH
from utils.SingleTask import Single_FENGYUN

sys.path.append(ROOT_PATH)

if __name__ == '__main__':
    print("现在开始多线程运行了")
    try:
        with ThreadPoolExecutor(max_workers=MAX_WORK_FENG_YUN, thread_name_prefix="pro") as pool:
            for line in range(RUNNING_OPTIONS_FENG_YUN):
                feature = pool.submit(Single_FENGYUN, str(line))
    except KeyboardInterrupt as e:
        print("手动终止了")
# pyinstaller.exe -F .\SiFang.py -p D:\aaaa\NikeRegister
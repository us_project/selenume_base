import os
import sys
from concurrent.futures import ThreadPoolExecutor

application_path = ""
if hasattr(sys, 'frozen'):
    application_path = os.path.dirname(sys.executable)
elif __file__:
    application_path = os.path.dirname(os.path.abspath(__file__))
sys.path.append(application_path)
print(f"当前项目路径为{application_path}")
# application_path = os.path.join(application_path, "SIFANG_PROJECT")
if not os.path.exists(application_path):
    os.mkdir(application_path)
if not os.path.exists(os.path.join(application_path, "Result")):
    os.mkdir(os.path.join(application_path, "Result"))
if not os.path.exists(os.path.join(application_path, "proxy")):
    os.mkdir(os.path.join(application_path, "proxy"))

if not os.path.exists(os.path.join(application_path, "logs")):
    os.mkdir(os.path.join(application_path, "logs"))



from SifangNew.Start import Single
from UtilsApi.proxyFileUtils import ProxyFileClass

proxyObj = ProxyFileClass(application_path)

if __name__ == '__main__':
    SIFANG_FILE_NAME = proxyObj.config["SIFANG_FILE_NAME"]
    with open(os.path.join(application_path,SIFANG_FILE_NAME),"r") as f:
        phoneList = f.read()

    listData = phoneList.split("\n")
    listPhone = list(filter(None,listData))
    print(f"当前所有手机号为{listPhone}")
    try:
        with ThreadPoolExecutor(max_workers=proxyObj.config["MAX_WORK_COUNT"], thread_name_prefix="pro") as pool:
            for phone in listPhone:
                pool.submit(lambda cxp: Single(*cxp), (application_path, proxyObj, phone))
    except KeyboardInterrupt as e:
        print("手动终止了")
# pyinstaller.exe -F .\Sifang_New.py -p D:\aaaa\NikeRegister

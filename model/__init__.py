import threading
import time
lock = threading.Lock()
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String
from utils.Constant import DbFilePath
from utils.ConstantEnum import CRUD_ENUM
from sqlalchemy.orm import sessionmaker
from utils.custumException import PhoneExistException
engine = create_engine(f'sqlite:///{DbFilePath}?check_same_thread=False', echo=True)
Base = declarative_base()


# 定义映射类User，其继承上一步创建的Base
class Phone(Base):
    # 指定本类映射到users表
    __tablename__ = 'phone'
    id = Column(Integer, primary_key=True, autoincrement=True)
    phonenumber = Column(String(20), unique=True)  # 手机号不可重复
    beginTime = Column(String(32))
    endTime = Column(String(32))
    comid = Column(String(32))
    code = Column(String(12))
    password = Column(String(22), default="")
    proxy = Column(String(12), default="")  # 谷歌浏览器使用的代码是哪个
    api = Column(String(12), default="")  # 当前使用的是哪个API
    error = Column(String(12), default="")  # 失败原因
    release = Column(String(12), default="0")  # 0 没有释放 1. 已释放 2.接口不需要释放
    status = Column(String(12), default="")
    # 1. 第一次注册成功 2. 改密码成功注册 3.失败 4.由于参数控制,手机不改密直接退出的 5. 程序正在运行中的


Base.metadata.create_all(engine, checkfirst=True)
# engine是2.2中创建的连接
Session = sessionmaker(bind=engine)


# 创建Session类实例
def getCurrentTime():
    """获取当前时间"""
    return time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time()))


def model_to_dict(modelData):
    data = [line.__dict__ for line in modelData]
    return data


def crudOperate(method=None, params=None):
    assert method, "请正确传入函数名和参数值"

    def SelectSingle(session, phone):

        return session.query(Phone).filter_by(phonenumber=phone).all()

    def SelectDataAll(session):
        mark = session.query(Phone).all()
        return mark
    def DeleteData(session, params):
        session.query(Phone).filter(Phone.phonenumber==params["phonenumber"]).delete()

    def UpdateData(session, params):
        phonenumber = params["phonenumber"]
        del params["phonenumber"]
        params["endTime"] = getCurrentTime()
        session.query(Phone).filter_by(phonenumber=phonenumber).update(params)

    def InsertData(session, params):

        mark = session.query(Phone).filter(Phone.phonenumber==params["phonenumber"]).all()
        if mark:
            print(f'当前手机号{params["phonenumber"]}已经存在,不需要注册了,直接开始下一个')
            raise PhoneExistException(f'当前手机号{params["phonenumber"]}已经存在,不需要注册了,直接开始下一个')
        params["beginTime"] = getCurrentTime()
        params["endTime"] = ""
        params["code"] = ""
        params["release"] = "0"
        ed_user = Phone(**params)
        session.add(ed_user)

    # SQLite 确保 变更数据的时候只有一个在更新
    result = None
    with lock:
        session = Session()
        if method == CRUD_ENUM.INSERT.value:
            InsertData(session, params)
        if method == CRUD_ENUM.UPDATE.value:
            UpdateData(session, params)
        if method == CRUD_ENUM.SELECT.value:
            result = SelectDataAll(session)
        if method == CRUD_ENUM.SELECTSINGLE.value:
            result = model_to_dict(SelectSingle(session, params))
        if method == CRUD_ENUM.DELETE.value:
            result = DeleteData(session, params)

        session.commit()
        session.close()
    return result

def getAllPhone():
    session = Session()
    mark = session.query(Phone).all()
    data = model_to_dict(mark)
    return [v["phonenumber"] for k, v in enumerate(data) if v["api"] =="SIFANG" and str(v["status"]) =="1"]

if __name__ == '__main__':
    print(getAllPhone())